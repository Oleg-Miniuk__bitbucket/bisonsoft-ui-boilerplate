/* eslint-disable no-unused-vars */

const path = require('path');
const commonWebpackModules = require('../../../../platf/config/webpack/commonWebpackModules');

module.exports = env => {
  const {
    module: { rules: commonRules },
    resolve: { alias: commonAliases },
    plugins: commonPlugins,
    devtool,
    optimization
  } = commonWebpackModules(env);

  const config = {
    mode: 'development',

    context: path.join(__dirname, '../../src'),

    entry: {
      bundle: './index.js'
    },

    output: {
      filename: '[name].js',
      chunkFilename: '[name].js',
      path: path.join(__dirname, '../../build_dev')
    },

    devServer: {
      contentBase: [path.join(__dirname, '../../build_dev')],
      port: 1337,
      historyApiFallback: true
    },

    resolve: {
      alias: {
        ...commonAliases
      }
    },

    module: {
      rules: [...commonRules]
    },
    plugins: [...commonPlugins],
    devtool,
    optimization
  };

  return config;
};
