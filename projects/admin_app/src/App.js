import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Modals from '@components/Modal/Modals';
import Loader from '@components/Loader/Loader';
import deviceUtils from '@utils/deviceUtils';
import withSuspense from '@hocs/withSuspense';
import PropTypes from 'prop-types';
import ControllersContext from '@contexts/controllers/ControllersContext';
import { connect } from 'react-redux';

const LoginPage = React.lazy(() => import('@admin_app/pages/LoginPage/LoginPage')
);

class App extends React.Component {
  async componentDidMount() {
    const { dispatch } = this.props;
    deviceUtils.initDevice(dispatch);
  }

  render() {
    return (
      <>
        <CssBaseline />
        <>
          {withSuspense(
            <Switch>
              <Route exact path="/" component={LoginPage} />
              <Route component={() => <Redirect to="/" />} />
            </Switch>
          )}
        </>
        {withSuspense(<Modals />)}
        <Loader />
      </>
    );
  }
}
App.contextType = ControllersContext;

App.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect(state => ({
  user: state.user
}))(App);
