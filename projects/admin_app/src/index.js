import React from 'react';
import ReactDOM from 'react-dom';
import initActions from '@admin_app/scripts/initActions';
import Launcher from '@admin_app/Launcher';

const { appControllers } = initActions();

ReactDOM.render(
  <Launcher appControllers={appControllers} />,
  document.querySelector('#root')
);
