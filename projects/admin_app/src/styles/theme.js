import muiTheme, { breakpoints, colors } from '@styles/theme';

const { width, up, down } = breakpoints;
const pxToRem = px => muiTheme.typography.pxToRem(px);

const theme = {
  ...muiTheme,
  colors,
  breakpoints: {
    xs: pxToRem(width('xs')),
    sm: pxToRem(width('sm')),
    md: pxToRem(width('md')),
    lg: pxToRem(width('lg')),
    xl: pxToRem(width('xl')),
    up,
    down
  }
};

export default theme;
