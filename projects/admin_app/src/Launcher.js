import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { BrowserRouter as BrowserRouterProvider } from 'react-router-dom';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/styles';
import { ThemeProvider } from 'styled-components';
import ControllersContext from '@contexts/controllers/ControllersContext';
import muiTheme from '@styles/theme';
import theme from '@admin_app/styles/theme';
import store from '@admin_app/store/store';
import '@icons/favicon.png';
import PropTypes from 'prop-types';
import App from './App';

const Launcher = ({ appControllers }) => (
  <>
    <BrowserRouterProvider>
      <ReduxProvider store={store}>
        <MuiThemeProvider theme={muiTheme}>
          <ThemeProvider theme={theme}>
            <ControllersContext.Provider value={appControllers}>
              <App />
            </ControllersContext.Provider>
          </ThemeProvider>
        </MuiThemeProvider>
      </ReduxProvider>
    </BrowserRouterProvider>
  </>
);

Launcher.propTypes = {
  appControllers: PropTypes.shape({
    modalsController: PropTypes.shape({}).isRequired,
    popupsController: PropTypes.shape({}).isRequired,
    gridsController: PropTypes.shape({}).isRequired
  }).isRequired
};

export default Launcher;
