import device from '@reducers/device/deviceReducer';
import modals from '@reducers/modals/modalsReducer';
import popups from '@reducers/popups/popupsReducer';
import grids from '@reducers/grids/gridsReducer';

const reducers = { device, modals, popups, grids };

export default reducers;
