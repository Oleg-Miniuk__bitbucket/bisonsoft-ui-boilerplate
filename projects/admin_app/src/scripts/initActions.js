import { dispatch, getState } from '@admin_app/store/store';
// import theme from './styles/theme';
import controllers from '@controllers/controllers';
import modalsList from '@admin_app/modules/ModulesConfigs/modalsList';
import popupsConfig from '@admin_app/modules/ModulesConfigs/popupsConfig';
import modalsConfig from '@admin_app/modules/ModulesConfigs/modalsConfig';
import popupsOptions from '@admin_app/modules/ModulesConfigs/popupsOptions';

const modalsController = controllers.getModalsController({
  dispatch,
  modalsList,
  modalsConfig
});

const popupsController = controllers.getPopupsController({
  dispatch,
  modalsController,
  popupsConfig,
  popupsOptions
});
const gridsController = controllers.getGridsController({
  dispatch,
  getState
});
const appControllers = {
  modalsController,
  popupsController,
  gridsController
};

const initActions = () => ({
  appControllers
});

export default initActions;
