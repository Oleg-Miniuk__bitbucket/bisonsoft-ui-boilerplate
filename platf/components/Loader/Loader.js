import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@units/Box/Box';
import loaderController from '@controllers/loader/loaderController';
import ReactDOM from 'react-dom';
import { Typography } from '@material-ui/core';

const StyledBack = styled(Box)`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9999;
  position: fixed;
  background-color: ${props => props.modalisopen && !props.forcedShowLoader ? 'transparent' : props.theme.colors.loaderBg};
`;

const ModalWrapper = styled(Box)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
`;
const StyledProgressLoader = styled(Typography).attrs({
  variant: 'inherit',
  component: 'p'
})`
  && {
    color: ${props => props.theme.colors.fontSecondary};
    background-color: ${props => props.theme.colors.primary};
    padding: .25rem .5rem;
    border-radius: 5px;
    margin-top: .5rem;
  }
`;
class Loader extends React.Component {
  loadingStartTimeout = null;

  minLoadingDelay = 100;

  shouldLoaderBeClosed = false;

  minLoaderLifeTime = 100;

  shouldShowLoader = () => {
    const {
      loader: { isPending, shouldShowLoader }
    } = this.props;
    if (isPending) {
      this.startLoadingTimeout();
    } else if (shouldShowLoader) {
      this.checkShouldLoaderBeClosed();
    } else {
      this.clearStartLoadingTimeout();
    }
  };

  checkShouldLoaderBeClosed = () => {
    const { dispatch } = this.props;

    if (this.shouldLoaderBeClosed) {
      loaderController.hideLoader(dispatch);
      this.shouldLoaderBeClosed = false;
    } else {
      setTimeout(() => this.checkShouldLoaderBeClosed(), 40);
    }
  };

  startLoadingTimeout = () => {
    const { dispatch } = this.props;

    this.loadingStartTimeout = setTimeout(() => {
      loaderController.showLoader(dispatch);
      setTimeout(() => {
        this.shouldLoaderBeClosed = true;
      }, this.minLoaderLifeTime);
    }, this.minLoadingDelay);
  };

  clearStartLoadingTimeout = () => {
    clearTimeout(this.loadingStartTimeout);
  };

  componentDidUpdate(prevProps) {
    const {
      loader: { isPending }
    } = this.props;
    if (isPending !== prevProps.loader.isPending) {
      this.shouldShowLoader();
    }
  }

  render() {
    const {
      modals,
      loader: { shouldShowLoader, progress },
      forcedShowLoader
    } = this.props;
    return ReactDOM.createPortal(
      <Box className="loader-wrapper">
        {(shouldShowLoader || forcedShowLoader) && (
          <StyledBack modalisopen={modals.length} forcedShowLoader={forcedShowLoader}>
            <ModalWrapper>
              <CircularProgress color="primary" />
              {typeof progress === 'number' && (
                <StyledProgressLoader>
                  {`${progress}%`}
                </StyledProgressLoader>
              )}
            </ModalWrapper>
          </StyledBack>
        )}
      </Box>,
      document.getElementById('loader-root')
    );
  }
}

Loader.propTypes = {
  loader: PropTypes.shape(),
  modals: PropTypes.instanceOf(Array),
  dispatch: PropTypes.func.isRequired,
  forcedShowLoader: PropTypes.bool,
};

Loader.defaultProps = {
  loader: {},
  modals: [],
  forcedShowLoader: false,
};

const enhance = compose(
  connect(state => ({
    loader: state.loader,
    modals: state.modals
  }))
);
export default enhance(Loader);
