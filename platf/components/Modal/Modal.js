import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Box from '@units/Box/Box';
import styled from 'styled-components';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ControllersContext from '@contexts/controllers/ControllersContext';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import ModalsCloser from './ModalsCloser';

const StyledPaper = styled(Paper)`
  padding-bottom: ${props => props.ismobile === 'true' ? '4rem !important' : 'inherit'
};
`;

const StyledBack = styled(Box)`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1101;
  position: fixed;
  background: ${props => props.theme.colors.modalBg};
  overflow-x: hidden;
  overflow-y: auto;
`;


const ModalScreenBack = styled(Box)`
  position: relative;
  display: table;
  height: 100%;
  width: 100%;
`;

const ModalContainer = styled(Box)`
  display: table-cell;
  vertical-align: middle;
  position: relative;
  padding: 1rem 0;
  text-align: center;
  @media (max-width: ${props => props.theme.breakpoints.sm}) {
    padding: 0;
  }
`;
const ModalContent = styled(Box)`
 position: relative;
 margin: 0 auto;
 display: inline-block;
 text-align: left;
`;
const ModalFormWrapper = styled(Box)`
 position: relative;
 padding: 2.5rem 0;

`;
class Modal extends Component {
  componentDidMount() {
    const { popupCode, modalId, device } = this.props;
    const { modalsController, popupsController } = this.context;
    const modalConfig = modalsController.getModalConfigById(modalId);
    const popupConfig = popupsController.getPopupConfigByCode(popupCode);
    if (popupConfig && popupConfig.onOpen) {
      popupConfig.onOpen({ ...this.props, modalsController, popupsController });
    }

    if (modalConfig && modalConfig.onOpen) {
      modalConfig.onOpen({ ...this.props, modalsController, popupsController });
    }
    if (this.backRef) {
      if (device.platform && device.platform.isMobile) {
        this.backRef.addEventListener('touchstart', this.handleClickOutside);
      } else {
        this.backRef.addEventListener('mousedown', this.handleClickOutside);
      }
    }
  }

  componentWillUnmount() {
    const { device } = this.props;
    if (this.backRef) {
      if (device.platform && device.platform.isMobile) {
        this.backRef.removeEventListener('touchstart', this.handleClickOutside);
      } else {
        this.backRef.removeEventListener('mousedown', this.handleClickOutside);
      }
    }
  }

  close = async cb => {
    const { modalId, popupCode, manualOnClose } = this.props;
    const { modalsController, popupsController, gridsController } = this.context;

    const modalConfig = modalsController.getModalConfigById(modalId);
    const popupConfig = popupsController.getPopupConfigByCode(popupCode);

    if (popupCode) {
      popupsController.deletePopup();
      if (popupConfig && popupConfig.onClose) {
        popupConfig.onClose({ ...this.props, modalsController, popupsController, gridsController });
      }
    }

    if (modalConfig && modalConfig.onClose) {
      modalConfig.onClose({ ...this.props, modalsController, popupsController, gridsController });
    }
    if (!manualOnClose) {
      modalsController.close(modalId);
    }
    if (typeof cb === 'function') {
      cb();
    }
  };

  handleClickOutside = event => {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.close();
    }
  };

  setWrapperRef = node => {
    this.wrapperRef = node;
  };

  setBackRef = node => {
    this.backRef = node;
  };

  render() {
    const {
      modalId,
      withoutCloser,
      history,
      device: {
        platform: {
          isMobile
        }
      },
      ...restModalProps
    } = this.props;
    const { modalsController } = this.context;
    const ModalForm = modalsController.getModalInstanceById(modalId);
    return (
      <StyledBack key={modalId}>
        <ModalScreenBack ref={this.setBackRef}>
          <ModalContainer>
            <ModalContent>
              <StyledPaper ismobile={`${isMobile}`}>
                <ModalFormWrapper ref={this.setWrapperRef} component={Container}>
                  {withoutCloser ? null : (
                    <ModalsCloser modalId={modalId} close={this.close} />
                  )}
                  <ModalForm
                    modalId={modalId}
                    close={this.close}
                    history={history}
                    {...restModalProps}
                  />
                </ModalFormWrapper>
              </StyledPaper>
            </ModalContent>
          </ModalContainer>
        </ModalScreenBack>
      </StyledBack>
    );
  }
}

Modal.contextType = ControllersContext;

Modal.propTypes = {
  modalId: PropTypes.string.isRequired,
  popupCode: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  history: PropTypes.shape(),
  withoutCloser: PropTypes.bool,
  manualOnClose: PropTypes.bool,
  device: PropTypes.shape(),
  modals: PropTypes.arrayOf(PropTypes.shape())
};

Modal.defaultProps = {
  history: {},
  popupCode: null,
  withoutCloser: false,
  manualOnClose: false,
  device: {},
  modals: []
};

const enhance = compose(
  connect(state => ({
    popupCode: state.popup,
    device: state.device,
    modals: state.modals,
    user: state.user
  }))
);

export default withRouter(enhance(Modal));
