import React, { Component } from 'react';
import Box from '@units/Box/Box';
import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Closer = styled(Box)`
  position: absolute;
  top: 0rem;
  right: 0rem;
  width: 3rem;
  height: 3rem;
  opacity: 0.3;
  cursor: pointer;

  &&:hover {
    opacity: 0.7;
    border-radius: '.3rem';
  }
  &&:before,
  &&:after {
    left: 50%;
    position: absolute;
    content: ' ';
    height: 0.2rem;
    margin-top: -.1rem;
    top: 50%;
    width: 1.4rem;
    margin-left: -.7rem;
    background-color: ${props => props.theme.colors.modalCloser};
  }
  &&:before {
    transform: rotate(45deg);
  }
  &&:after {
    transform: rotate(-45deg);
  }
`;

class ModalsCloser extends Component {
  componentDidMount() {
    if (this.closerRef) {
      this.closerRef.addEventListener('click', this.handleClick);
    }
  }

  componentWillUnmount() {
    this.closerRef.removeEventListener('click', this.handleClick);
  }

  handleClick = () => {
    const { close } = this.props;
    if (close && typeof close === 'function') {
      close();
    }
  };

  setCloserRef = node => {
    this.closerRef = node;
  };

  render() {
    return <Closer ref={this.setCloserRef} />;
  }
}

ModalsCloser.propTypes = {
  close: PropTypes.func.isRequired
};

export default ModalsCloser;
