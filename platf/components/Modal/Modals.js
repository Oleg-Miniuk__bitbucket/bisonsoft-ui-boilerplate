import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Modal from './Modal';

class Modals extends Component {
  componentDidUpdate(prevProps) {
    const { modals } = this.props;
    if (modals.length !== prevProps.modals.length) {
      this.handleFreezeROOTDocument();
    }
  }

  handleFreezeROOTDocument = async () => {
    const { modals } = this.props;
    const BODY = document.body;
    if (modals.length === 1) {
      BODY.style.overflow = 'hidden';
    } else if (modals.length === 0) {
      BODY.style = '';
    }
  }

  render() {
    const { modals } = this.props;
    return modals.map(modal => {
      const { modalId } = modal;
      return (
        <Modal key={modalId} {...modal} />
      );
    });
  }
}

Modals.propTypes = {
  modals: PropTypes.arrayOf(PropTypes.object)
};

Modals.defaultProps = {
  modals: []
};

const ConnectedModals = connect(state => ({
  modals: state.modals
}))(Modals);

export default () => ReactDOM.createPortal(
  <ConnectedModals />,
  document.getElementById('modal-root')
);
