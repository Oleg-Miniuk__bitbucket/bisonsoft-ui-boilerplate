import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconSucceess from '@icons/success.svg';
import InlineSVG from 'svg-inline-react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { Grid as GridLayout } from '@material-ui/core/';

const StyledSVG = styled(InlineSVG).attrs({
  src: IconSucceess
})`
  height: 3rem;
  width: 3rem;
  margin: 0 auto;
  display: block;
  && path {
    fill: ${props => props.theme.colors.success};
  }
`;
const PopupSucccess = ({ title, text, close }) => (
  <Container maxWidth="xs" fixed>
    <GridLayout container spacing={4}>
      <GridLayout item container spacing={2}>
        <GridLayout item xs={12}>
          <StyledSVG />
        </GridLayout>
        <GridLayout item xs={12}>
          <Typography component="h3" variant="h4" align="center">
            {title}
          </Typography>
        </GridLayout>
        <GridLayout item xs={12}>
          <Typography component="p" align="center">
            {text}
          </Typography>
        </GridLayout>
      </GridLayout>
      <GridLayout item container spacing={2} justify="center">
        <GridLayout item>
          <Button color="primary" variant="contained" onClick={close}>
            OK
          </Button>
        </GridLayout>
      </GridLayout>
    </GridLayout>
  </Container>
);

PopupSucccess.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]).isRequired,
  close: PropTypes.func.isRequired
};

const enhance = compose(connect(), withRouter);
export default enhance(PopupSucccess);
