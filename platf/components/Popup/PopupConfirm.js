import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconWarning from '@icons/warning.svg';
import InlineSVG from 'svg-inline-react';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import { Grid as GridLayout } from '@material-ui/core/';
import ControllersContext from '@contexts/controllers/ControllersContext';

const StyledSVG = styled(InlineSVG).attrs({
  src: IconWarning
})`
  height: 3rem;
  width: 3rem;
  margin: 0 auto;
  display: block;
`;


const PopupConfirm = ({
  title,
  text,
  confirmButtonText,
  rejectButtonText,
  popupCode,
  close,
  Icon,
  ...restProps
}) => {
  const { popupsController, modalsController, gridsController } = useContext(ControllersContext);
  const popupConfig = popupsController.getPopupConfigByCode(popupCode);
  const confirm = () => {
    popupConfig.onConfirm({ ...restProps, popupsController, modalsController, gridsController, close });
  };
  const reject = () => {
    popupConfig.onReject({ ...restProps, popupsController, modalsController, gridsController, close });
  };
  return (
    <Container maxWidth="xs" fixed>
      <GridLayout container spacing={4}>
        <GridLayout item container spacing={2}>
          <GridLayout item xs={12}>
            {Icon ? <Icon /> : <StyledSVG />}
          </GridLayout>
          <GridLayout item xs={12}>
            <Typography component="h3" variant="h4" align="center">
              {title}
            </Typography>
          </GridLayout>
          <GridLayout item xs={12}>
            <Typography component="p" align="center">{text}</Typography>
          </GridLayout>
        </GridLayout>
        <GridLayout item container spacing={2} justify="space-between">
          <GridLayout item>
            <Button
              variant="contained"
              color="default"
              onClick={reject}
            >
              {rejectButtonText}
            </Button>
          </GridLayout>
          <GridLayout item>
            <Button autoFocus color="primary" variant="contained" onClick={confirm}>
              {confirmButtonText}
            </Button>
          </GridLayout>
        </GridLayout>
      </GridLayout>
    </Container>
  );
};

PopupConfirm.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]).isRequired,
  popupCode: PropTypes.number.isRequired,
  confirmButtonText: PropTypes.string.isRequired,
  rejectButtonText: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  Icon: PropTypes.shape({})
};
PopupConfirm.defaultProps = {
  Icon: null
};

const enhance = compose(
  connect(),
  withRouter
);
export default enhance(PopupConfirm);
