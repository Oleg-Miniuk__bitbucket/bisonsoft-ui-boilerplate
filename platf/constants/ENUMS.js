const cardStatusValues = {
  CARD_STATUS_CHECK: 0,
  CARD_STATUS_EMPTY: 1,
  CARD_STATUS_INPROGRESS: 2,
  CARD_STATUS_APPROVED: 3,
  CARD_STATUS_REJECTED: 4
};
const cardStatusNames = {
  CARD_STATUS_CHECK: 'К проверке',
  CARD_STATUS_EMPTY: 'Не заполнено',
  CARD_STATUS_INPROGRESS: 'На доработке',
  CARD_STATUS_APPROVED: 'Одобрено',
  CARD_STATUS_REJECTED: 'Не допущен'
};
const regStatusValues = {
  REG_STATUS_CHOOSE_RES: 0,
  REG_STATUS_DO_QUIZ: 1,
  REG_STATUS_FILL_CARD: 2,
  REG_STATUS_CARD_CHECKING: 3,
  REG_STATUS_DONE: 4
};
const operationNameByType = {
  10: 'Покупка Stable coin',
  20: 'Обмен Stable coin/ICO',
  30: 'Приобрeтение ICO',
  40: 'Обмен ICO/Stable coin',
  50: 'Вывод Stable coin',
  60: 'Окончание срока размещения ICO',
  70: 'Получение процентного дохода за ICO'
};
const requestOperationByStatus = {
  10: 'Заявка создана',
  20: 'Заявка в обработке',
  30: 'Сумма заблокирована',
  40: 'Отправлена в СДБО',
  50: 'Принята в СДБО',
  60: 'Отклонена в СДБО',
  70: 'Операция проведена в СДБО',
  80: 'Операция отменена в СДБО',
  90: 'Отменена системой',
  100: 'Отменена пользователем',
  110: 'Выполнена',
  120: 'Отменена'
};
export { cardStatusValues, cardStatusNames, regStatusValues, operationNameByType, requestOperationByStatus };
