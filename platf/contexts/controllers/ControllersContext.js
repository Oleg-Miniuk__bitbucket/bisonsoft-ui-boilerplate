import React from 'react';

const ControllersContext = React.createContext({
  modalsController: () => {},
  popupsController: () => {},
  gridsController: () => {}
});

export default ControllersContext;
