import createBreakpoints from '@material-ui/core/styles/createBreakpoints';
import './fonts/fonts.css';
import { createMuiTheme } from '@material-ui/core/styles';

const breakpoints = createBreakpoints({});
const globalTheme = createMuiTheme();
const {
  spacing,
  typography: { pxToRem }
} = globalTheme;
// all props https://material-ui.com/customization/default-theme/

const colors = {
  error: '#F54B5E',
  warning: '#F8C51C',
  success: '#48D2A0',
  primary: '#057FAD',
  accent: '#0794CA',
  fontMain: '#47474B',
  fontSecondary: '#ffffff',
  fontDisabled: '#9B9B9B',
  modalBg: 'rgba(0, 0, 0, 0.5)',
  modalCloser: '#333',
  loaderBg: 'rgba(0, 0, 0, 0.2)',
  gridBorderColor: '#e0e0e0',
  lightBackground: '#FFFFFF',
  lightMediumBackground: '#F1F1F1',
  mediumBackground: '#47474B',
  lightDarkBackground: '#2D2B2B',
  darkBackground: '#1F1E1E',
  mainBannerBG: '#ffffff',
  dividerColor: 'rgba(0,0,0,0.12)',
  linearProgressBarBack: '#E9E9E9',
  linearProgressBarFront: '#2B2B2B'
};

const theme = createMuiTheme({
  // example custom use
  palette: {
    primary: {
      main: '#0794CA',
      contrastText: '#FFF'
    },
    secondary: {
      main: '#fff',
      contrastText: '#0794CA'
    },
    text: {
      primary: '#47474B',
      secondary: '#fff',
      disabled: '#9B9B9B',
    },
    error: {
      main: '#F54B5E'
    }
  },
  background: {
    default: '#fff',
  },

  overrides: {
    MuiContainer: {
      root: {
        maxWidth: '100vw'
      }
    },

    MuiCssBaseline: {
      '@global': {
        body: {
          backgroundColor: '#fff',
          fontSize: '1.125rem',
          lineHeight: 1.2,
          [breakpoints.down('xs')]: {
            fontSize: '.875rem',
            lineHeight: 1.42
          },
        },
        img: {
          maxWidth: '100%',
          width: 'auto',
          maxHeight: '100%',
          height: 'auto'
        },
        strong: {
          fontWeight: 500
        },
        b: {
          fontWeight: 700
        },
        'input[type="number"]': {
          '-moz-appearance': 'textfield'
        },
        'input[type=number]::-webkit-inner-spin-button': {
          '-webkit-appearance': 'none',
          margin: 0
        },
        'input[type=number]::-webkit-outer-spin-button': {
          '-webkit-appearance': 'none',
          margin: 0
        }
      }
    },
    MuiFormLabel: {
      root: {
        color: '#47474B'
      }
    },
    MuiCheckbox: {
      root: {
        color: '#0794CA'
      }
    },
    MuiButton: {
      root: {
        fontSize: '.875rem',
        borderRadius: '5px',
        // backgroundColor: '#ECECEC',
        lineHeight: 1.125,
        minHeight: '40px',
        '&:hover': {
          backgroundColor: '#fff'
        },
        '&$disabled': {
          border: 'none'
        }
      },
      text: {
        padding: '.5rem 1.5rem',
      },
      containedPrimary: {
        backgroundColor: '#057FAD',
        '&:hover': {
          backgroundColor: '#057FAD',
          '@media (hover: none)': {
            backgroundColor: '#057FAD',
          },
        }
      },
      containedSecondary: {
        border: 'solid 2px #0794CA',
        '&:hover': {
          // color: 'inherit',
          backgroundColor: 'inherit',
          // backgroundColor: '#e0e0e0',
          '@media (hover: none)': {
            color: 'inherit',
          },
        }
      },
      outlinedPrimary: {
        color: '#0794CA',
        border: '2px solid #057FAD',
        borderRadius: '5px',
        backgroundColor: '#fff',
        boxShadow: 'none',
        '&:hover': {
          border: '2px solid #057FAD',
          backgroundColor: '#fff'
        },
      },
      outlined: {
        backgroundColor: '#057FAD',
        border: 'none',
        color: '#FFF',
        '&:hover': {
          backgroundColor: '#057FAD',
          // Reset on touch devices, it doesn't add specificity
          '@media (hover: none)': {
            backgroundColor: '#057FAD',
          },
        }
      },
      sizeLarge: {
        minHeight: '70px',
        padding: '.5rem 1rem',
        fontSize: '1.25rem',
        fontWeight: '500',
        borderRadius: '5px',
        border: 0,
        '&:hover': {
          border: 0
        },
        [breakpoints.down('xs')]: {
          padding: '.5rem 1.5rem',
          minHeight: '64px',
          borderRadius: '6px'
        },
      }
    },
    MuiFlatPageButton: {
      root: {
        backgroundColor: 'transparent'
      }
    },
    MuiPickersToolbarButton: {
      toolbarBtn: {
        backgroundColor: 'transparent',
        '&:hover': {
          backgroundColor: 'inherit',
        }
      }
    },
    MuiFormHelperText: {
      root: {
        color: 'inherit'
      }
    },
    MuiRadio: {
      root: {
        color: 'inherit',
      },
      colorPrimary: {
        color: '#057FAD'
      }
    },
    MuiStepLabel: {
      label: {
        color: 'inherit'
      }
    },
    MuiStepIcon: {
      root: {
        '&$completed': {
          color: '#48D2A0',
        },
      },
      completed: {
        color: '#48D2A0'
      }
    },
    MuiInputBase: {
      input: {
        lineHeight: 1
      }
    }
  },
  typography: {
    fontFamily: 'Roboto, sans-serif',
    body1: {

    },
    h1: {
      fontSize: '3.75rem',
      lineHeight: 1.166,
      letterSpacing: '.134rem',
      fontWeight: 'normal',
      color: '#47474B',
      [breakpoints.up('xl')]: {
        fontSize: '5.9375rem'
      },
      [breakpoints.down('sm')]: {
        fontSize: '1.875rem',
        lineHeight: 1.16,
        letterSpacing: '.05rem',
      },
      [breakpoints.down('xs')]: {
        fontSize: '1.75rem',
        lineHeight: 1.17
      },
    },
    h2: {
      fontSize: '3.25rem',
      lineHeight: 1.166,
      letterSpacing: '.134rem',
      fontWeight: 'normal',
      [breakpoints.down('sm')]: {
        fontSize: '1.75rem',
      },
      [breakpoints.down('xs')]: {
        fontSize: '1.5rem',
      }
    },
    h3: {
      fontSize: '1.875rem',
      lineHeight: '1.625rem',
      textTransform: 'uppercase',
      fontWeight: 500,
      color: '#2B2B2B',
      [breakpoints.up('xl')]: {
        fontSize: '2rem'
      },
      [breakpoints.down('sm')]: {
        fontSize: '1.5rem',
        lineHeight: 1.08
      }
    },
    h4: {
      [breakpoints.down('sm')]: {
        fontSize: '1.725rem'
      },
      [breakpoints.down('xs')]: {
        fontSize: '1.275rem'
      }
    },
    h5: {
      [breakpoints.down('sm')]: {
        fontSize: '1.325rem'
      },
      [breakpoints.down('xs')]: {
        fontSize: '1rem'
      }
    },
    subtitle1: {
      fontWeight: 500,
      fontSize: '1.25rem',
      lineHeight: 1.3,
      color: '#2B2B2B',
      [breakpoints.down('xs')]: {
        fontSize: '.875rem',
        lineHeight: 1.4
      }
    },
  }
});


export { colors, breakpoints, spacing, pxToRem };
export default theme;
