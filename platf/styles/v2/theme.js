// TODO new fonts or prev
// all props https://material-ui.com/customization/default-theme/

import '../fonts/fonts.css';
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import createBreakpoints from '@material-ui/core/styles/createBreakpoints';

const breakpoints = createBreakpoints({});
const globalTheme = createMuiTheme();
const { spacing, typography: { pxToRem } } = globalTheme;
const { typography } = responsiveFontSizes(globalTheme, {
  variants: ['h1', 'h2', 'h3', 'h4', 'h5'],
});

const colors = {
  primary: '#4A4A4A',
  secondary: '#56B3EC',
  disabled: fade('#9B9B9B', 0.3),
  warning: '#FFab00',
  error: '#F44336',
  success: '#8bc34a',
  lightContrast: '#FFF',
  darkContrast: '#000',
  defaultText: '#5D5D5D',
  primaryText: '#2B2B2B',
  secondaryText: '#FFF',
  disabledText: '#9B9B9B',
  default: '#FFF',
  inputBorderColor: '#9B9B9B',
  inputPlaceholderColor: '#9B9B9B',
  inputHelperText: '#9B9B9B'
};

const shadows = {
  switchShadow: `0px 2px 2px ${fade('#000000', 0.237602)}, 0px 0px 2px ${fade('#000000', 0.12)}`
};

const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary,
      contrastText: colors.lightContrast
    },
    secondary: {
      main: colors.secondary,
      contrastText: colors.lightContrast
    },
    warning: {
      main: colors.warning,
      contrastText: colors.darkContrast
    },
    error: {
      main: colors.error,
      contrastText: colors.lightContrast
    },
    success: {
      main: colors.success,
      contrastText: colors.darkContrast
    },
    text: {
      primary: colors.primaryText,
      secondary: colors.secondaryText,
      disabled: colors.disabledText
    }
  },
  typography: {
    ...typography,
    h6: {
      fontSize: pxToRem(20),
      lineHeight: 1.111,
      textTransform: 'uppercase',
      fontWeight: 400,
      [breakpoints.down('md')]: {
        fontSize: pxToRem(16),
        lineHeight: 1.25
      },
      [breakpoints.down('xs')]: {
        fontSize: pxToRem(14),
        lineHeight: 1.142
      }
    },
    subtitle1: {
      fontSize: pxToRem(14),
      lineHeight: 1.14,
      textTransform: 'uppercase',
      fontWeight: 400
    },
    subtitle2: {
      fontSize: pxToRem(14),
      lineHeight: 1.14,
      fontWeight: 400
    },
    body1: {
      fontSize: pxToRem(16),
      lineHeight: 1.25
    },
    body2: {
      fontSize: pxToRem(10),
      lineHeight: 1.1
    }
  },
  overrides: {
    '@global': {
      img: {
        maxWidth: '100%',
        width: 'auto',
        maxHeight: '100%',
        height: 'auto',
        display: 'block',
      },
      strong: {
        fontWeight: 500
      },
      b: {
        fontWeight: 700
      }
    },
    MuiCheckbox: {
      colorPrimary: {
        color: colors.primary,
        '&$checked': {
          color: colors.secondary
        },
        '&$disabled': {
          color: colors.disabled
        }
      },
      colorSecondary: {
        '&$checked': {
          color: colors.secondary
        },
        '&$disabled': {
          color: colors.disabled
        }
      }
    },
    MuiButton: {
      root: {
        borderRadius: '2px'
      },
      contained: {
        backgroundColor: colors.default
      },
      containedSecondary: {
        '&$disabled': {
          color: colors.disabledText,
          backgroundColor: colors.disabled
        }
      },
      outlinedSecondary: {
        '&, &:hover': {
          color: colors.default,
          borderColor: colors.default,
          backgroundColor: 'initial'
        }
      }
    },
    MuiToolbar: {
      root: {
        alignItems: 'stretch'
      }
    },
    MuiFormLabel: {
      root: {
        color: colors.inputPlaceholderColor,
        '&$focused:not($error)': {
          color: colors.secondary
        }
      },
      colorSecondary: {
        '&$error': {
          color: colors.error
        },
        '&$focused:not($error)': {
          color: colors.secondary
        }
      }
    },
    MuiFormControlLabel: {},
    MuiRadio: {
      root: {
        color: colors.secondary,
        '&$disabled': {
          color: colors.disabled
        }
      },
      colorPrimary: {
        color: colors.primary,
        '&$checked': {
          color: colors.secondary
        },
        '&$disabled': {
          color: colors.disabled
        }
      },
      colorSecondary: {
        color: colors.default,
        '&$checked': {
          color: colors.secondary
        },
        '& + .MuiFormControlLabel-label': {
          color: colors.secondaryText
        },
        '&$disabled': {
          color: colors.disabled
        },
        '&$disabled + .MuiFormControlLabel-label': {
          color: colors.disabledText
        }
      }
    },
    MuiSwitch: {
      root: {
        '& $track, &$disabled + $track': {
          backgroundColor: colors.disabled,
          opacity: 1
        }
      },
      switchBase: {
        color: colors.default,
        '&$disabled': {
          color: colors.disabledText
        }
      },
      thumb: {
        boxShadow: shadows.switchShadow
      },

      colorPrimary: {
        '&$checked': {
          color: colors.secondary
        },
        '&$checked + $track': {
          backgroundColor: colors.secondary,
        },
        '&$disabled': {
          color: colors.disabledText
        },
        '&$disabled + $track': {
          backgroundColor: colors.disabled,
          opacity: 1
        }
      },
      colorSecondary: {
        '&$disabled': {
          color: colors.disabledText
        },
        '&$disabled + $track': {
          backgroundColor: colors.disabled,
          opacity: 1
        }
      },
    },
    MuiInputBase: {
      input: {
        color: colors.primaryText,
        caretColor: colors.secondary
      },
      colorSecondary: {
        '& $input': {
          color: colors.default
        },
        '&$disabled $input': {
          color: colors.disabledText
        }
      },
    },
    MuiInput: {
      underline: {
        '&:before': {
          borderBottomColor: colors.inputBorderColor
        },
        '&:after': {
          borderBottomColor: colors.secondary
        },
        '&:hover:not($disabled):before': {
          borderBottomColor: colors.secondary
        }
      }
    },
    MuiOutlinedInput: {
      notchedOutline: {
        borderColor: colors.inputBorderColor
      },
      root: {
        '&:hover:not($error), &$focused:not($error)': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: colors.secondary
          }
        },
        '&$disabled .MuiOutlinedInput-notchedOutline': {
          borderColor: colors.inputBorderColor
        }
      },
      colorSecondary: {
        '&$focused&$error': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: colors.error
          }
        }
      }
    },
    MuiFormHelperText: {
      root: {
        color: colors.inputHelperText
      }
    },
    MuiButtonGroup: {
      grouped: {
        fontSize: pxToRem(10),
        border: `1px solid ${colors.primary}`,
        boxShadow: 'none'
      }
    },
    MuiTabs: {
      root: {
        '& $indicator': {
          height: '3px'
        }
      },

    },
    MuiTab: {
      root: {
        color: colors.defaultText,
        paddingTop: '15px',
        paddingBottom: '18px',
        minWidth: 'auto',
        [breakpoints.up('xs')]: {
          minWidth: 'auto'
        },
        '& svg': {
          '& path': {
            fill: colors.defaultText
          }
        },
        '&$disabled': {
          color: colors.disabledText,
          '& svg': {
            '& path': {
              fill: fade(colors.disabledText, 0.8)
            }
          },
        }
      },
      wrapper: {
        fontSize: 0
      },
      textColorPrimary: {
        color: colors.defaultText,
        '&$selected': {
          color: colors.primaryText,
          '& svg': {
            '& path': {
              fill: colors.primaryText
            }
          }
        },
        '&:hover:not($disabled)': {
          color: colors.primaryText,
          '& svg': {
            '& path': {
              fill: colors.primaryText
            }
          }
        }
      },
      textColorSecondary: {
        '& svg': {
          '& path': {
            fill: colors.secondaryText
          }
        },
        '&$selected': {
          color: colors.secondaryText,
          '& svg': {
            '& path': {
              fill: colors.secondaryText
            }
          }
        },
        '&:hover:not($disabled)': {
          color: colors.secondary,
          '& svg': {
            '& path': {
              fill: colors.secondary
            }
          }
        }
      }
    },
    MuiBreadcrumbs: {
      root: {
        color: colors.defaultText,
        '&.MuiTypography-colorTextSecondary': {
          color: colors.secondaryText,
        }
      },
      separator: {
        fontSize: pxToRem(12),
        marginLeft: spacing(0.5),
        marginRight: spacing(0.5)
      },
    }
  },
});

export { colors, breakpoints, spacing, pxToRem };
export default theme;
