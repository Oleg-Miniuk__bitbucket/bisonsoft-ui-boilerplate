const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const commonWebpackModules = (env = {}) => ({
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true
        }
      },
      {
        test: /\.(png|jpg|gif|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'images/[name].[ext]' }
          }
        ]
      },
      {
        test: /\.(ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'fonts/[name].[ext]' }
          }
        ]
      },
      {
        test: /\.(pdf)$/,
        use: [
          {
            loader: 'file-loader',
            options: { name: 'documents/[name].[ext]' }
          }
        ]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]'
            }
          }
        ]
      },
      { test: /\.svg$/, loader: 'svg-inline-loader' }
    ]
  },
  resolve: {
    alias: {
      '@units': path.join(__dirname, '../../units'),
      '@utils': path.join(__dirname, '../../utils'),
      '@components': path.join(__dirname, '../../components'),
      '@controllers': path.join(__dirname, '../../controllers'),
      '@constants': path.join(__dirname, '../../constants'),
      '@reducers': path.join(__dirname, '../../reducers'),
      '@contexts': path.join(__dirname, '../../contexts'),
      '@icons': path.join(__dirname, '../../icons'),
      '@styles': path.join(__dirname, '../../styles'),
      '@hocs': path.join(__dirname, '../../hocs'),
      '@config': path.join(__dirname, '../../config'),
      '@admin_app': path.join(__dirname, '../../../projects/admin_app/src')
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: env.production
        ? path.join(__dirname, '../../public/prod/index.html')
        : path.join(__dirname, '../../public/dev/index.html'),
      filename: 'index.html',
      inject: 'body'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(
        env.production ? 'production' : 'development'
      ),
      'process.env.SERVER_ENV': JSON.stringify(env.server),
      'process.env.API_ENV': JSON.stringify(
        env.local_api ? 'local' : 'dev_machine'
      )
    })
  ],
  devtool: env.production ? 'none' : 'source-map',
  optimization: {
    splitChunks: {
      chunks: 'all',
      minSize: 30000,
      maxSize: 0,
      minChunks: 1,
      automaticNameDelimiter: '_',
      maxAsyncRequests: 7,
      maxInitialRequests: 5,
      automaticNameMaxLength: 30,
      name: true,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
          reuseExistingChunk: true
        }
      }
    }
  }
});

module.exports = commonWebpackModules;
