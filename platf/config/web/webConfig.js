const serverMachineConfig = {
  // QA
  QA_APP_HOST: 'https://testapp.finstore.by',
  QA_API_HOST: 'https://testapi.finstore.by',
  QA_API_PATH: '/api/v1',
  QA_SOCKET_HOST: 'ws://testapp.finstore.by',
  // DEV
  DEV_APP_HOST: 'https://app.dev.finstore.by',
  DEV_API_HOST: 'https://api.dev.finstore.by',
  DEV_API_PATH: '/api/v1',
  DEV_SOCKET_HOST: 'ws://dev.finstore.by',
  // PRE-PROD
  PRE_PROD_APP_HOST: 'https://app.pp.finstore.by',
  PRE_PROD_API_HOST: 'https://api.pp.finstore.by',
  PRE_PROD_API_PATH: '/api/v1',
  PRE_PROD_SOCKET_HOST: 'ws://app.pp.finstore.by',
  // PROD
  PROD_APP_HOST: 'https://app.finstore.by',
  PROD_API_HOST: 'https://api.finstore.by',
  PROD_API_PATH: '/api/v1',
  PROD_SOCKET_HOST: 'ws://app.finstore.by'
};

// TODO update local dev server configs
const localMachineConfig = {
  QA_PORT: 50829,
  QA_API_HOST: 'http://localhost',
  API_PATH: '/api/v1',
  SOCKET_QA_API_HOST: 'ws://localhost',
  SOCKET_API_PATH: ''
};

const config = process.env.API_ENV === 'local' ? localMachineConfig : serverMachineConfig;
export default config;
