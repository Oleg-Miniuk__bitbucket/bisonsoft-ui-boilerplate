import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import gridsReducer from './gridsReducer';


describe('grids reducer', () => {
  it('GRID_INIT (init new grid', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_INIT,
      grid: {
        gridId: 'gridId3'
      }
    };

    const initialState = [{
      gridId: 'gridId1'
    },
    {
      gridId: 'gridId2'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_INIT);

    expect(action.grid).toBeDefined();

    expect(typeof action.grid).toBe('object');

    expect(initialState).toEqual(
      expect.not.arrayContaining(
        [
          action.grid
        ]
      )
    );

    expect(gridsReducer(initialState, action)).toEqual([
      ...initialState,
      action.grid
    ]);
  });

  it('GRID_CLOSE (remove grid)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_CLOSE,
      gridId: 'gridId6'
    };

    const initialState = [{
      gridId: 'gridId1'
    },
    {
      gridId: 'gridId2'
    },
    {
      gridId: 'gridId3'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_CLOSE);

    expect(action.gridId).toBeDefined();

    expect(typeof action.gridId).toBe('string');

    expect(gridsReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          gridId: action.gridId
        })
      ])
    );
  });

  it('GRID_CLOSE_ALL (remove all grids)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_CLOSE_ALL
    };

    const initialState = [{
      gridId: 'gridId1'
    },
    {
      gridId: 'gridId2'
    },
    {
      gridId: 'gridId3'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_CLOSE_ALL);

    expect(gridsReducer(initialState, action)).toHaveLength(0);
  });

  it('GRID_CHANGE_FIELDS (change fields grid)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_CHANGE_FIELDS,
      gridId: 'gridId2',
      fields: {
        value: 11
      }
    };

    const initialState = [{
      gridId: 'gridId1',
      value: 1
    },
    {
      gridId: 'gridId2',
      value: 1
    },
    {
      gridId: 'gridId3',
      value: 1
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_CHANGE_FIELDS);

    expect(action.gridId).toBeDefined();

    expect(typeof action.gridId).toBe('string');

    expect(action.fields).toBeDefined();

    expect(typeof action.fields).toBe('object');

    expect(gridsReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.objectContaining(
          {
            gridId: action.gridId,
            ...action.fields
          })
      ])
    );
  });
});
