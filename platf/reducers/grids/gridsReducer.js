import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const gridsReducer = (state = [], action) => {
  if (action.type === COMMON_CONSTANTS.GRID_INIT) {
    return [...state, action.grid];
  }
  if (action.type === COMMON_CONSTANTS.GRID_CLOSE) {
    return state.filter(grid => grid.gridId !== action.gridId);
  }
  if (action.type === COMMON_CONSTANTS.GRID_CLOSE_ALL) {
    return [];
  }
  if (action.type === COMMON_CONSTANTS.GRID_CHANGE_FIELDS) {
    return state.map(grid => {
      if (grid.gridId === action.gridId) {
        return {
          ...grid,
          ...action.fields
        };
      }
      return grid;
    });
  }
  return state;
};

export default gridsReducer;
