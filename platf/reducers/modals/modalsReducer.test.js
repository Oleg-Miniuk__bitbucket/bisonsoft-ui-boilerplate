import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import modalsReducer from './modalsReducer';


describe('modals reducer', () => {
  it('MODAL_INIT (init modal)', () => {
    const action = {
      type: COMMON_CONSTANTS.MODAL_INIT,
      modal: {
        modalId: 'modalId3'
      }
    };

    const initialState = [{
      modalId: 'modalId1'
    },
    {
      modalId: 'modalId2'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.MODAL_INIT);

    expect(action.modal).toBeDefined();

    expect(typeof action.modal).toBe('object');

    expect(initialState).toEqual(
      expect.not.arrayContaining(
        [
          action.modal
        ]
      )
    );

    expect(modalsReducer(initialState, action)).toEqual([
      ...initialState,
      action.modal
    ]);
  });

  it('MODAL_CLOSE (close modal)', () => {
    const action = {
      type: COMMON_CONSTANTS.MODAL_CLOSE,
      modalId: 'modalId6'
    };

    const initialState = [{
      modalId: 'modalId1'
    },
    {
      modalId: 'modalId2'
    },
    {
      modalId: 'modalId3'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.MODAL_CLOSE);

    expect(action.modalId).toBeDefined();

    expect(typeof action.modalId).toBe('string');

    expect(modalsReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          modalId: action.modalId
        })
      ])
    );
  });

  it('MODAL_CLOSE_ALL (close all modals)', () => {
    const action = {
      type: COMMON_CONSTANTS.MODAL_CLOSE_ALL
    };

    const initialState = [{
      modalId: 'modalId1'
    },
    {
      modalId: 'modalId2'
    },
    {
      modalId: 'modalId3'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.MODAL_CLOSE_ALL);

    expect(modalsReducer(initialState, action)).toHaveLength(0);
  });

  it('MODAL_CHANGE_FIELDS (change fields modal)', () => {
    const action = {
      type: COMMON_CONSTANTS.MODAL_CHANGE_FIELDS,
      modalId: 'modalId2',
      fields: {
        value: 11
      }
    };

    const initialState = [{
      modalId: 'modalId1',
      value: 1
    },
    {
      modalId: 'modalId2',
      value: 1
    },
    {
      modalId: 'modalId3',
      value: 1
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.MODAL_CHANGE_FIELDS);

    expect(action.modalId).toBeDefined();

    expect(typeof action.modalId).toBe('string');

    expect(action.fields).toBeDefined();

    expect(typeof action.fields).toBe('object');

    expect(modalsReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.objectContaining(
          {
            modalId: action.modalId,
            ...action.fields
          })
      ])
    );
  });
});
