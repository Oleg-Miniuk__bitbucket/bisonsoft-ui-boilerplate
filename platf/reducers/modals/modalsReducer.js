import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const modalsReducer = (state = [], action) => {
  if (action.type === COMMON_CONSTANTS.MODAL_INIT) {
    return [...state, action.modal];
  }
  if (action.type === COMMON_CONSTANTS.MODAL_CLOSE) {
    return state.filter(modal => modal.modalId !== action.modalId);
  }
  if (action.type === COMMON_CONSTANTS.MODAL_CLOSE_ALL) {
    return [];
  }
  if (action.type === COMMON_CONSTANTS.MODAL_CHANGE_FIELDS) {
    return state.map(modal => {
      if (modal.modalId === action.modalId) {
        return {
          ...modal,
          ...action.fields
        };
      }
      return modal;
    });
  }
  return state;
};

export default modalsReducer;
