import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const loaderReducer = (state = {}, action) => {
  if (action.type === COMMON_CONSTANTS.APP_LOADING) {
    return {
      ...state,
      isPending: action.status
    };
  }
  if (action.type === COMMON_CONSTANTS.APP_LOADED) {
    return {
      ...state,
      isPending: action.status
    };
  }
  if (action.type === COMMON_CONSTANTS.HIDE_LOADER) {
    return {
      ...state,
      shouldShowLoader: action.status,
      progress: null
    };
  }
  if (action.type === COMMON_CONSTANTS.SHOW_LOADER) {
    return {
      ...state,
      shouldShowLoader: action.status
    };
  }
  if (action.type === COMMON_CONSTANTS.PROGRESS_LOADER) {
    return {
      ...state,
      progress: action.progress
    };
  }
  return state;
};

export default loaderReducer;
