import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import loaderReducer from './loaderReducer';


describe('loader reducer', () => {
  it('APP_LOADING (set app loading)', () => {
    const action = {
      type: COMMON_CONSTANTS.APP_LOADING,
      status: true
    };

    const initialState = {
      isPending: false
    };

    expect(action.type).toBe(COMMON_CONSTANTS.APP_LOADING);

    expect(action.status).toBeDefined();

    expect(typeof action.status).toBe('boolean');

    expect(action.status).toBeTruthy();

    expect(loaderReducer(initialState, action)).toEqual({
      ...initialState,
      isPending: action.status
    });
  });

  it('APP_LOADED (set app loaded)', () => {
    const action = {
      type: COMMON_CONSTANTS.APP_LOADED,
      status: false
    };

    const initialState = {
      isPending: true
    };

    expect(action.type).toBe(COMMON_CONSTANTS.APP_LOADED);

    expect(action.status).toBeDefined();

    expect(typeof action.status).toBe('boolean');

    expect(action.status).toBeFalsy();

    expect(loaderReducer(initialState, action)).toEqual({
      ...initialState,
      isPending: action.status
    });
  });

  it('HIDE_LOADER (hide global loader) ', () => {
    const action = {
      type: COMMON_CONSTANTS.HIDE_LOADER,
      status: false
    };

    const initialState = {
      shouldShowLoader: true,
      progress: null
    };

    expect(action.type).toBe(COMMON_CONSTANTS.HIDE_LOADER);

    expect(action.status).toBeDefined();

    expect(typeof action.status).toBe('boolean');

    expect(action.status).toBeFalsy();

    expect(loaderReducer(initialState, action)).toEqual({
      ...initialState,
      shouldShowLoader: action.status
    });
  });

  it('SHOW_LOADER (show global loader) ', () => {
    const action = {
      type: COMMON_CONSTANTS.SHOW_LOADER,
      status: true
    };

    const initialState = {
      shouldShowLoader: false
    };

    expect(action.type).toBe(COMMON_CONSTANTS.SHOW_LOADER);

    expect(action.status).toBeDefined();

    expect(typeof action.status).toBe('boolean');

    expect(action.status).toBeTruthy();

    expect(loaderReducer(initialState, action)).toEqual({
      ...initialState,
      shouldShowLoader: action.status
    });
  });
});
