import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const userReducer = (state = {}, action) => {
  if (action.type === COMMON_CONSTANTS.IS_RESIDENT) {
    return {
      ...state,
      isResident: action.isResident
    };
  }
  if (action.type === COMMON_CONSTANTS.TOKEN) {
    return {
      ...state,
      token: action.token
    };
  }
  if (action.type === COMMON_CONSTANTS.CURRENT_USER) {
    return {
      ...state,
      current: action.current
    };
  }
  return state;
};

export default userReducer;
