
import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import userReducer from './userReducer';


describe('user reducer', () => {
  it('IS_RESIDENT (set user is resident)', () => {
    const action = {
      type: COMMON_CONSTANTS.IS_RESIDENT,
      isResident: true
    };

    const initialState = {
      isResident: false
    };
    expect(action.type).toBe(COMMON_CONSTANTS.IS_RESIDENT);

    expect(action.isResident).toBeDefined();

    expect(typeof action.isResident).toBe('boolean');

    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      isResident: action.isResident
    });
  });

  it('TOKEN (set user token)', () => {
    const action = {
      type: COMMON_CONSTANTS.TOKEN,
      token: 'token'
    };
    const initialState = {
      token: null
    };

    expect(action.type).toBe(COMMON_CONSTANTS.TOKEN);

    expect(action.token).toBeDefined();

    expect(typeof action.token).toBe('string');

    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      token: action.token
    });
  });

  it('CURRENT_USER (set current user props)', () => {
    const action = {
      type: COMMON_CONSTANTS.CURRENT_USER,
      current: {
        id: '1'
      }
    };
    const initialState = {
      current: null
    };
    expect(action.type).toBe(COMMON_CONSTANTS.CURRENT_USER);

    expect(action.current).toBeDefined();

    expect(typeof action.current).toBe('object');

    expect(userReducer(initialState, action)).toEqual({
      ...initialState,
      current: action.current
    });
  });
});
