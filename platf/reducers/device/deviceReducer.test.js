import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import deviceReducer from '@reducers/device/deviceReducer';

describe('deviceReducer', () => {
  it('PLATFORM', () => {
    const initialState = { width: 1920 };
    const action = {
      type: COMMON_CONSTANTS.PLATFORM,
      platform: { isBrowser: true }
    };
    expect(deviceReducer(initialState, action)).toEqual({
      ...initialState,
      platform: { isBrowser: true }
    });
  });

  it('WIDTH', () => {
    const initialState = { platform: { isBrowser: true }, height: 1080 };
    const action = {
      type: COMMON_CONSTANTS.WIDTH,
      width: 1920
    };
    expect(deviceReducer(initialState, action)).toEqual({
      ...initialState,
      width: 1920
    });
  });

  it('HEIGHT', () => {
    const initialState = { platform: { isBrowser: true }, width: 1920 };
    const action = {
      type: COMMON_CONSTANTS.HEIGHT,
      height: 1080
    };
    expect(deviceReducer(initialState, action)).toEqual({
      ...initialState,
      height: 1080
    });
  });

  it('type: undefined', () => {
    const initialState = {};
    const action = {
      type: undefined,
      something: 'some content'
    };
    expect(deviceReducer(initialState, action)).toEqual(initialState);
  });

  it('type: null', () => {
    const initialState = {};
    const action = {
      type: null
    };
    expect(deviceReducer(initialState, action)).toEqual(initialState);
  });
});
