import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const deviceReducer = (state = {}, action) => {
  if (action.type === COMMON_CONSTANTS.PLATFORM) {
    return {
      ...state,
      platform: action.platform
    };
  }

  if (action.type === COMMON_CONSTANTS.WIDTH) {
    return {
      ...state,
      width: action.width
    };
  }
  if (action.type === COMMON_CONSTANTS.HEIGHT) {
    return {
      ...state,
      height: action.height
    };
  }
  return state;
};

export default deviceReducer;
