import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const popupReducer = (popupCode = 0, action) => {
  if (action.type === COMMON_CONSTANTS.SET_POPUP_STATE) {
    return action.popupCode;
  }
  if (action.type === COMMON_CONSTANTS.CLEAR_POPUP_STATE) {
    return 0;
  }
  return popupCode;
};

export default popupReducer;
