import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import popupsReducer from './popupsReducer';


describe('popups reducer', () => {
  it('SET_POPUP_STATE (set popup code)', () => {
    const action = {
      type: COMMON_CONSTANTS.SET_POPUP_STATE,
      popupCode: 20
    };
    const initialState = {
      popupCode: 0
    };

    expect(action.type).toBe(COMMON_CONSTANTS.SET_POPUP_STATE);

    expect(action.popupCode).toBeDefined();

    expect(typeof action.popupCode).toBe('number');

    expect(action.popupCode).toBeGreaterThanOrEqual(0);

    expect(popupsReducer(initialState, action)).toEqual(action.popupCode);
  });

  it('CLEAR_POPUP_STATE (clear popup code)', () => {
    const action = {
      type: COMMON_CONSTANTS.CLEAR_POPUP_STATE
    };
    const initialState = {
      popupCode: 20
    };

    expect(action.type).toBe(COMMON_CONSTANTS.CLEAR_POPUP_STATE);

    expect(popupsReducer(initialState, action)).toEqual(0);
  });
});
