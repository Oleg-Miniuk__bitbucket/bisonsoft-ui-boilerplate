import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import gridsFiltersReducer from './gridsFiltersReducer';


describe('gridsFilter reducer', () => {
  it('GRID_FILTER_INIT (init filters grid)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_FILTER_INIT,
      gridFilter: {
        value: 'newValue'
      }
    };

    const initialState = [{
      value: 'value 1'
    },
    {
      value: 'value 2'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_FILTER_INIT);

    expect(action.gridFilter).toBeDefined();

    expect(typeof action.gridFilter).toBe('object');

    expect(initialState).toEqual(
      expect.not.arrayContaining(
        [
          action.gridFilter
        ]
      )
    );

    expect(gridsFiltersReducer(initialState, action)).toEqual([
      ...initialState,
      action.gridFilter
    ]);
  });

  it('GRID_FILTER_CHANGE (change filter grid)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_FILTER_CHANGE,
      gridFilter: {
        gridId: 'gridId2',
        value: 11,
        labeL: 'new label'
      }
    };

    const initialState = [{
      gridId: 'gridId1',
      value: 1,
      labeL: 'label 1'
    },
    {
      gridId: 'gridId2',
      value: 2,
      labeL: 'label 2'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_FILTER_CHANGE);

    expect(action.gridFilter).toBeDefined();

    expect(typeof action.gridFilter).toBe('object');

    expect(action.gridFilter.gridId).toBeDefined();

    expect(typeof action.gridFilter.gridId).toBe('string');

    expect(gridsFiltersReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.objectContaining(
          {
            ...action.gridFilter
          })
      ])
    );
  });

  it('GRID_FILTER_REMOVE (close modal)', () => {
    const action = {
      type: COMMON_CONSTANTS.GRID_FILTER_REMOVE,
      gridFilter: {
        gridId: 'gridId2'
      }
    };

    const initialState = [{
      gridId: 'gridId1',
      value: 1,
      labeL: 'label 1'
    },
    {
      gridId: 'gridId2',
      value: 2,
      labeL: 'label 2'
    }];

    expect(action.type).toBe(COMMON_CONSTANTS.GRID_FILTER_REMOVE);

    expect(action.gridFilter.gridId).toBeDefined();

    expect(typeof action.gridFilter.gridId).toBe('string');

    expect(gridsFiltersReducer(initialState, action)).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          gridId: action.gridFilter.gridId
        })
      ])
    );
  });
});
