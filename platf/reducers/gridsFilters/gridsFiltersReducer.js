import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const gridsFiltersReducer = (state = [], action) => {
  if (action.type === COMMON_CONSTANTS.GRID_FILTER_INIT) {
    return [...state, action.gridFilter];
  }
  if (action.type === COMMON_CONSTANTS.GRID_FILTER_CHANGE) {
    return state.map(grid => {
      if (grid.gridId === action.gridFilter.gridId) {
        return {
          ...action.gridFilter
        };
      }
      return grid;
    });
  }
  if (action.type === COMMON_CONSTANTS.GRID_FILTER_REMOVE) {
    return state.filter(
      gridFilter => gridFilter.gridId !== action.gridFilter.gridId
    );
  }
  return state;
};

export default gridsFiltersReducer;
