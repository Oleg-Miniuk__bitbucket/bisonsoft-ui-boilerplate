import modalsController from './modals/modalsController';
import popupsController from './popups/popupsController';
import gridsController from './grids/gridsController';

const controllers = {
  getModalsController(...args) {
    return modalsController.getModalsController(...args);
  },
  getPopupsController(...args) {
    return popupsController.getPopupsController(...args);
  },
  getGridsController(...args) {
    return gridsController.getGridsController(...args);
  }
};

export default controllers;
