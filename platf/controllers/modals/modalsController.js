import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const modalsController = {
  modalsController: {
    _dispatch: () => {},
    _modalsList: {},
    _modalsConfig: {},

    getModalInstanceById(modalId) {
      return this._modalsList[modalId];
    },
    getModalFromReduxById(state, modalId) {
      return state.modals.find(modal => modal.modalId === modalId);
    },
    getModalConfigById(modalId) {
      return this._modalsConfig[modalId];
    },

    async init(modalId, options = {}) {
      this._dispatch({
        type: COMMON_CONSTANTS.MODAL_INIT,
        modal: {
          modalId,
          ...options
        }
      });
    },
    async close(modalId) {
      this._dispatch({
        type: COMMON_CONSTANTS.MODAL_CLOSE,
        modalId
      });
    },
    async closeAll() {
      this._dispatch({
        type: COMMON_CONSTANTS.MODAL_CLOSE_ALL
      });
    },
    async changeFields({ modalId, fields }) {
      this._dispatch({
        type: COMMON_CONSTANTS.MODAL_CHANGE_FIELDS,
        modalId,
        fields
      });
    }
  },

  getModalsController({ dispatch, modalsList, modalsConfig }) {
    this.modalsController._dispatch = dispatch;
    this.modalsController._modalsList = modalsList;
    this.modalsController._modalsConfig = modalsConfig;
    return Object.create(this.modalsController);
  }
};

export default modalsController;
