import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const loaderController = {
  async setAppPending(dispatch) {
    await dispatch({
      type: COMMON_CONSTANTS.APP_LOADING,
      status: true
    });
  },
  async showLoader(dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.SHOW_LOADER,
      status: true
    });
  },
  async unSetAppPending(dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.APP_LOADED,
      status: false
    });
  },
  async hideLoader(dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.HIDE_LOADER,
      status: false,
      progress: null
    });
  },
  async progessLoader({ dispatch, progress }) {
    dispatch({
      type: COMMON_CONSTANTS.PROGRESS_LOADER,
      progress
    });
  }
};

export default loaderController;
