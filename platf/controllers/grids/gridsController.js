import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import loaderController from '@controllers/loader/loaderController';

const gridsController = {
  gridsController: {
    _dispatch: () => { },
    _getState: () => { },

    getGridFromReduxById(grids, gridId) {
      return grids.find(grid => grid.gridId === gridId);
    },
    getGridFromState(gridId) {
      const { grids } = this._getState();
      return this.getGridFromReduxById(grids, gridId);
    },
    async init(gridId, options) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_INIT,
        grid: {
          gridId,
          ...options
        }
      });
    },
    async close(gridId) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_CLOSE,
        gridId
      });
    },
    async closeAll() {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_CLOSE_ALL
      });
    },
    async changeFields({ gridId, fields }) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_CHANGE_FIELDS,
        gridId,
        fields
      });
    },
    async initGridFilter(gridId) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_FILTER_INIT,
        gridFilter: {
          gridId,
          columns: []
        }
      });
    },
    async changeGridFilter(gridId, columns) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_FILTER_CHANGE,
        gridFilter: {
          gridId,
          columns
        }
      });
    },
    async removeGridFilter(gridId) {
      this._dispatch({
        type: COMMON_CONSTANTS.GRID_FILTER_REMOVE,
        gridFilter: {
          gridId
        }
      });
    },
    async getGrid({ gridId, requestBody, queryParams = {}, requestApi, getCustomItems }) {
      try {
        const grid = this.getGridFromState(gridId);
        const { PageSize, currentPage, sorting } = { ...grid, ...queryParams };
        const respData = await requestApi({
          requestBody,
          queryParams: {
            PageSize: PageSize || null,
            Page: currentPage || currentPage === 0 ? currentPage + 1 : null,
            sorting: sorting ? `${sorting.column}:${sorting.type}` : null
          }
        });
        const { data, paging = { limit: null, page: null, totalPages: null } } = respData;
        this.init(gridId, {
          items: getCustomItems ? getCustomItems(data) : data,
          PageSize: paging.limit,
          currentPage: paging.page - 1,
          totalPages: paging.totalPages,
          sorting
        });
      } catch (error) {
        throw new Error(error);
      }
    },
    async updateGrid({
      gridId,
      requestBody = {},
      queryParams = {},
      requestApi,
      getCustomItems
    }) {
      loaderController.setAppPending(this._dispatch);
      try {
        const grid = this.getGridFromState(gridId);
        const { PageSize, currentPage, sorting } = { ...grid, ...queryParams };
        const respData = await requestApi({
          requestBody,
          queryParams: {
            PageSize: PageSize || null,
            Page: currentPage ? currentPage + 1 : null,
            sorting: sorting ? `${sorting.column}:${sorting.type}` : null
          }
        });
        const { data, paging = { limit: null, page: null, totalPages: null } } = respData;
        await this.changeFields({
          gridId,
          fields: {
            items: getCustomItems ? getCustomItems(data) : data,
            PageSize: paging.limit,
            currentPage:
              paging.page > paging.totalPages
                ? paging.totalPages - 1
                : paging.page - 1,
            totalPages: paging.totalPages,
            sorting
          }
        });
        loaderController.unSetAppPending(this._dispatch);
      } catch (error) {
        loaderController.unSetAppPending(this._dispatch);
        throw new Error(error);
      }
    }
  },
  getGridsController({ dispatch, getState }) {
    this.gridsController._dispatch = dispatch;
    this.gridsController._getState = getState;
    return Object.create(this.gridsController);
  }
};

export default gridsController;
