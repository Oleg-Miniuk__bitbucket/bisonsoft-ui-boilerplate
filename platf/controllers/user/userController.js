import api from '@controllers/api/api';
import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';
import { regStatusValues } from '@constants/ENUMS';

const userController = {
  async getCurrentUserData(dispatch) {
    const userDataResp = await api.currentUser();
    const {
      data: { data: userData, status }
    } = userDataResp;
    if (status === 10) {
      dispatch({
        type: COMMON_CONSTANTS.CURRENT_USER,
        current: userData
      });
    }
    return status;
  },
  getUserRegStatus({ current }) {
    if (current && Object.keys(current).length !== 0) {
      if (current.isResident === null) return regStatusValues.REG_STATUS_CHOOSE_RES;
      if (!current.isQuizPassed) return regStatusValues.REG_STATUS_DO_QUIZ;
      if (
        current.isResident !== null
        && current.isQuizPassed === true
        && current.isProfileUpdated !== true
        && current.isAllowFinOperation === null
      ) {
        return regStatusValues.REG_STATUS_FILL_CARD;
      }
      if (
        current.isResident !== null
        && current.isQuizPassed === true
        && current.isProfileUpdated === true
        && current.isProfileChecked !== true
      ) {
        return regStatusValues.REG_STATUS_CARD_CHECKING;
      }
      if (
        current.isResident !== null
        && current.isQuizPassed === true
        && current.isProfileUpdated === true
        && current.isAllowFinOperation !== null
      ) {
        return regStatusValues.REG_STATUS_DONE;
      }
    }
    return false;
  }
};

export default userController;
