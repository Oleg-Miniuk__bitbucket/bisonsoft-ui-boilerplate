import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const PopupsController = {
  PopupsController: {
    _dispatch: () => { },
    _modalsController: () => { },
    _popupsOptions: {},
    _popupsConfig: {},

    async initPopup(popupCode = 1, customOptions = {}) {
      this._dispatch({
        type: COMMON_CONSTANTS.SET_POPUP_STATE,
        popupCode
      });

      const popupContent = Object.assign(
        this._popupsOptions[popupCode],
        customOptions
      );

      this._modalsController.init(COMMON_CONSTANTS[popupContent.type], {
        ...popupContent
      });
    },

    async deletePopup() {
      this._dispatch({
        type: COMMON_CONSTANTS.CLEAR_POPUP_STATE
      });
    },

    getPopupConfigByCode(popupCode) {
      return this._popupsConfig[popupCode];
    }
  },

  getPopupsController({
    dispatch,
    modalsController,
    popupsOptions = {},
    popupsConfig
  }) {
    this.PopupsController._dispatch = dispatch;
    this.PopupsController._modalsController = modalsController;
    this.PopupsController._popupsOptions = popupsOptions;
    this.PopupsController._popupsConfig = popupsConfig;
    return Object.create(this.PopupsController);
  }
};

export default PopupsController;
