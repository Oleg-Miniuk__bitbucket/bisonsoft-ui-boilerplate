import React from 'react';
import Loader from '@components/Loader/Loader';

const withSuspense = Module => (
  <React.Suspense fallback={<Loader forcedShowLoader />}>
    {
      typeof Module === 'object'
        ? Module
        : <Module />
    }
  </React.Suspense>
);

export default withSuspense;
