const modalUtils = {
  getModalFromReduxById(state, modalId) {
    return state.modals.find(modal => modal.modalId === modalId);
  }
};

export default modalUtils;
