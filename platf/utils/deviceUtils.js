import { deviceDetect } from 'react-device-detect';
import COMMON_CONSTANTS from '@constants/COMMON_CONSTANTS';

const deviceUtils = {
  initDevice(dispatch) {
    let width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
    let height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    window.addEventListener('resize', () => {
      width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
      height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
      this.setWidth(width, dispatch);
      this.setHeight(height, dispatch);
    });

    window.addEventListener('orientationchange', () => {
      width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;
      height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;
      this.setWidth(width, dispatch);
      this.setHeight(height, dispatch);
    });
    this.setPlatform(deviceDetect(), dispatch);
    this.setWidth(width, dispatch);
    this.setHeight(height, dispatch);
  },

  async setWidth(width, dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.WIDTH,
      width
    });
  },

  async setHeight(height, dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.HEIGHT,
      height
    });
  },

  async setPlatform(platform, dispatch) {
    dispatch({
      type: COMMON_CONSTANTS.PLATFORM,
      platform
    });
  }
};

export default deviceUtils;
