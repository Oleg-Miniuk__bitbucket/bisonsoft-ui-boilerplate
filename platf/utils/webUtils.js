import webConfig from '../config/web/webConfig';

const { DEV_PORT, DEV_HOST, API_PATH } = webConfig;

const webUtils = {
  getBasePath() {
    const {
      location: { protocol, hostname }
    } = window;
    if (process.env.NODE_ENV === 'production') {
      return `${protocol}//${hostname}`;
    }
    return `${DEV_HOST}`;
  },
  getApiPath() {
    const {
      location: { port }
    } = window;
    if (process.env.NODE_ENV === 'production') {
      return `${this.getBasePath()}:${port}${API_PATH}`;
    }
    return `${this.getBasePath()}:${DEV_PORT}${API_PATH}`;
  }
};
export default webUtils;
