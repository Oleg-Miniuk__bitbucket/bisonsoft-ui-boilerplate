import React, { Component } from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { colors } from '@styles/theme';
import ControllersContext from '@contexts/controllers/ControllersContext';

const styles = {
  multiValue: base => ({ ...base, backgroundColor: colors.primary }),
  multiValueLabel: base => ({ ...base, color: colors.lightContrast }),
  multiValueRemove: base => ({
    ...base,
    color: colors.lightContrast,
    ':hover': {
      backgroundColor: colors.error
    }
  })
};

class FilterColumns extends Component {
  async componentDidMount() {
    const { gridId } = this.props;
    const { gridsController } = this.context;
    gridsController.initGridFilter(gridId);
  }

  componentWillUnmount() {
    const { gridsController } = this.context;
    const { gridId } = this.props;
    gridsController.removeGridFilter(gridId);
  }

  createOptions = () => {
    const { columns } = this.props;

    const options = columns.map(column => ({
      ...column,
      label: column.title,
      value: column.columnId
    }));
    return options;
  };

  defaultValue = () => {
    const { columns } = this.props;
    return this.createOptions(columns);
  };

  onChange = async value => {
    const { gridsController } = this.context;
    const { gridId } = this.props;

    gridsController.changeGridFilter(gridId, value);
  };

  render() {
    const { placeholder, noOptionsMessage } = this.props;
    return (
      <Select
        isMulti
        // defaultValue={this.defaultValue()}
        isSearchable={false}
        styles={styles}
        className="basic-multi-select"
        classNamePrefix="select"
        onChange={this.onChange}
        options={this.defaultValue()}
        placeholder={placeholder}
        noOptionsMessage={() => noOptionsMessage}
      />
    );
  }
}
FilterColumns.contextType = ControllersContext;

FilterColumns.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  gridId: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  noOptionsMessage: PropTypes.string
};
FilterColumns.defaultProps = {
  placeholder: 'Выберите колонку таблицы',
  noOptionsMessage: 'Выбранны все колонки'
};
export default FilterColumns;
