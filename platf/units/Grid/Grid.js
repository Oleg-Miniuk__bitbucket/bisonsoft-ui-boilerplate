import React from 'react';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import FlatPagination from 'material-ui-flat-pagination';
import styled from 'styled-components';
import Box from '@units/Box/Box';
import Flex from '@units/Flex/Flex';
import Typography from '@material-ui/core/Typography';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutline from '@material-ui/icons/HelpOutline';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import ControllersContext from '@contexts/controllers/ControllersContext';
import FilterColumns from './FilterColumns';

const StyledTable = styled(Table)`
  border-collapse: collapse;
  width: 100%;
  @media (max-width: ${props => props.theme.breakpoints.md}) {
    display: block;
    thead,
    tbody {
      display: block;
    }
    thead {
      position: absolute;
      top: -9999px;
      left: -9999px;
      height: 0;
      width: 0;
      overflow: hidden;
    }
  }
`;

const StyledTypography = styled(Typography)`
  && {
    line-height: 1.24;
  }
`;

const StyledHeader = styled(StyledTypography)`
  && {
    font-weight: 700;
  }
`;

const StyledTableSortLabel = styled(TableSortLabel)`
  && {
    font-size: 1rem;
    font-weight: 700;
    @media (max-width: ${props => props.theme.breakpoints.md}) {
      align-items: flex-start;
    }
    svg {
      fill: ${props => props.theme.colors.fontMain};
    }
  }
`;

const StyledTr = styled(Tr)`
  vertical-align: top;
  &&:first-child td {
    padding-top: 1.5rem;
  }
  @media (max-width: ${props => props.theme.breakpoints.md}) {
    display: flex;
    flex-wrap: wrap;
    border: 1px solid ${props => props.theme.colors.gridBorderColor};
    padding: 0.5rem 0;
    + tr {
      margin-top: 1rem;
    }
  }
  @media (max-width: ${props => props.theme.breakpoints.sm}) {
    display: block;
  }
  &&:nth-child(even) td:not(.actions) {
    background-color: rgba(155, 155, 155, 0.03);
    @media (max-width: ${props => props.theme.breakpoints.md}) {
      background-color: transparent;
    }
  }
`;

const StyledTh = styled(Th)`
  border-bottom: 1px solid ${props => props.theme.colors.gridBorderColor};
  padding: 1rem;
  min-width: 7rem;
  display: ${props => (props.ishidden === 'true' ? 'none' : false)};
  &:last-child {
    padding-right: 0;
  }
`;
const StyledActionTh = styled(StyledTh)`
  padding: 0;
  border: 0;
  min-width: auto;
`;
const StyledTd = styled(Td)`
  padding: 1rem;
  display: ${props => (props.ishidden === 'true' ? 'none !important' : false)};
  .tdBefore {
    display: none;
    line-height: 1.24rem;
    margin-bottom: 0.5rem;
  }
  @media (max-width: ${props => props.theme.breakpoints.md}) {
    flex: 50% 1 0;
    display: block;
    border: none;
    padding: 0.5rem 1rem;
    .tdBefore {
      display: block;
    }
  }
  @media (max-width: ${props => props.theme.breakpoints.sm}) {
    padding: 0.5rem 1rem;
  }
`;
const StyledActionTd = styled(StyledTd)`
  border: 0;
  && {
    background-color: transparent;
  }
  .tdBefore {
    display: none;
  }
  @media (max-width: ${props => props.theme.breakpoints.md}) {
    flex: 100% 1 0;
  }
  @media (max-width: ${props => props.theme.breakpoints.sm}) {
    padding: 0.5rem 1rem;
  }
`;

const HiddenActionTd = styled(StyledActionTd)`
  height: 0;
  padding: 0;
  div {
    display: none;
  }
`;

const StyledTableWrapper = styled(Box).attrs({
  width: '100%',
  overflow: 'auto visible'
})``;

const tableTdProps = {
  variant: 'body1',
  align: 'left',
  component: 'p'
};

class Grid extends React.Component {
  handlePaginationClick = (e, newOffset) => {
    const { handlePaginationClick } = this.props;
    if (handlePaginationClick) handlePaginationClick(newOffset);
  };

  handleSortingColumn = () => {
    const { grid, changeSortingColumn } = this.props;
    const {
      sorting: { type: sortingType }
    } = grid;
    changeSortingColumn(sortingType);
  };

  shouldHideColumn = columnId => {
    const { gridFilters, filterColumns, columns } = this.props;

    if (filterColumns && gridFilters) {
      const visibleColumnsFromFilter = gridFilters.columns
        && gridFilters.columns.find(
          gridFilter => gridFilter.columnId === columnId
        );
      if (visibleColumnsFromFilter) return 'false';
      const isFixedColumns = columns.find(
        column => column.columnId === columnId && column.isFixed
      );

      if (isFixedColumns) return 'false';
      return 'true';
    }
    return 'false';
  };

  render() {
    const {
      grid,
      columns,
      user,
      filterColumns,
      gridId,
      customItemId,
      notFoundText
    } = this.props;
    const {
      items,
      totalPages,
      currentPage,
      sorting: { column: sortingColumn, type: sortingType } = {}
    } = grid;
    return (
      <Box>
        {items && items.length > 0 && columns.length ? (
          <>
            {filterColumns && (
              <Box mb=".5rem">
                <FilterColumns
                  columns={columns.filter(column => !column.isFixed)}
                  changeColumns={this.visibleColumns}
                  gridId={gridId}
                  placeholder="Добавить колонки"
                />
              </Box>
            )}
            <StyledTableWrapper>
              <StyledTable>
                <Thead>
                  <StyledTr>
                    {columns.map(
                      ({
                        columnId,
                        title: columnTitle,
                        type: columnType,
                        tooltipText,
                        TooltipIcon
                      }) => columnType !== 'actions' ? (
                        <StyledTh
                          key={columnId}
                          ishidden={this.shouldHideColumn(columnId)}
                        >
                          {sortingColumn !== columnId ? (
                            <>
                              {tooltipText ? (
                                <Tooltip
                                  title={
                                    typeof tooltipText === 'function'
                                      ? tooltipText()
                                      : (() => (
                                        <>
                                          <Typography>
                                            {tooltipText}
                                          </Typography>
                                        </>
                                      ))()
                                  }
                                  enterTouchDelay={30}
                                  leaveTouchDelay={30}
                                >
                                  <Flex>
                                    <StyledHeader {...tableTdProps}>
                                      {columnTitle}
                                    </StyledHeader>
                                    {TooltipIcon ? (
                                      <Box pl=".25rem">
                                        <TooltipIcon />
                                      </Box>
                                    ) : (
                                      <Box pl=".25rem">
                                        <HelpOutline />
                                      </Box>
                                    )}
                                  </Flex>
                                </Tooltip>
                              ) : (
                                <StyledHeader {...tableTdProps}>
                                  {columnTitle}
                                </StyledHeader>
                              )}
                            </>
                          ) : (
                            <>
                              {tooltipText ? (
                                <Tooltip
                                  title={tooltipText}
                                  enterTouchDelay={30}
                                  leaveTouchDelay={30}
                                >
                                  <Flex>
                                    <StyledTableSortLabel
                                      active
                                      direction={sortingType}
                                      onClick={this.handleSortingColumn}
                                      {...tableTdProps}
                                    >
                                      {columnTitle}
                                    </StyledTableSortLabel>
                                    {TooltipIcon ? (
                                      <TooltipIcon />
                                    ) : (
                                      <HelpOutline />
                                    )}
                                  </Flex>
                                </Tooltip>
                              ) : (
                                <StyledTableSortLabel
                                  active
                                  direction={sortingType}
                                  onClick={this.handleSortingColumn}
                                  {...tableTdProps}
                                >
                                  {columnTitle}
                                </StyledTableSortLabel>
                              )}
                            </>
                          )}
                        </StyledTh>
                      ) : (
                        columnType === 'actions' && (
                          <StyledActionTh
                            key={columnId}
                            ishidden={this.shouldHideColumn(columnId)}
                          >
                            {tooltipText && (
                              <Tooltip
                                title={tooltipText}
                                enterTouchDelay={30}
                                leaveTouchDelay={30}
                              >
                                <Flex>
                                  <StyledHeader {...tableTdProps}>
                                    {columnTitle}
                                  </StyledHeader>
                                  {TooltipIcon ? (
                                    <TooltipIcon />
                                  ) : (
                                    <HelpOutline />
                                  )}
                                </Flex>
                              </Tooltip>
                            )}
                          </StyledActionTh>
                        )
                      )
                    )}
                  </StyledTr>
                </Thead>
                <Tbody>
                  {items.map(item => (
                    <StyledTr key={item[customItemId]}>
                      {columns.map(
                        ({
                          columnId,
                          type: columnType,
                          ActionComponent,
                          itemFormatter,
                          formatterActionComponent
                        }) => columnType !== 'actions' ? (
                          <StyledTd
                            key={columnId}
                            ishidden={this.shouldHideColumn(columnId)}
                          >
                            <StyledTypography {...tableTdProps}>
                              {itemFormatter
                                ? itemFormatter(item)
                                : item[columnId]}
                            </StyledTypography>
                          </StyledTd>
                        ) : formatterActionComponent ? (
                          formatterActionComponent(item) ? (
                            <StyledActionTd
                              key={columnId}
                              className="actions"
                              ishidden={this.shouldHideColumn(columnId)}
                            >
                              {formatterActionComponent(item)}
                            </StyledActionTd>
                          ) : (
                            <HiddenActionTd key={columnId} />
                          )
                        ) : (
                          <StyledActionTd
                            key={columnId}
                            className="actions"
                            ishidden={this.shouldHideColumn(columnId)}
                          >
                            <ActionComponent item={item} user={user} />
                          </StyledActionTd>
                        )
                      )}
                    </StyledTr>
                  ))}
                </Tbody>
              </StyledTable>
            </StyledTableWrapper>
          </>
        ) : (
          <Typography>{notFoundText}</Typography>
        )}
        {totalPages > 1 && (
          <Flex centered mt=".5rem">
            <FlatPagination
              offset={currentPage}
              limit={1}
              total={totalPages || 0}
              onClick={this.handlePaginationClick}
              currentPageColor="primary"
              otherPageColor="inherit"
            />
          </Flex>
        )}
      </Box>
    );
  }
}
Grid.contextType = ControllersContext;

Grid.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      type: PropTypes.string
    })
  ),
  gridFilters: PropTypes.shape({
    gridId: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.shape())
  }),
  grid: PropTypes.shape({
    gridId: PropTypes.string,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        type: PropTypes.string
      })
    ),
    totalPages: PropTypes.number,
    columns: PropTypes.arrayOf(PropTypes.shape())
  }),
  sorting: PropTypes.shape({
    column: PropTypes.string,
    type: PropTypes.string
  }),
  user: PropTypes.shape(),
  handlePaginationClick: PropTypes.func,
  changeSortingColumn: PropTypes.func,
  gridId: PropTypes.string,
  filterColumns: PropTypes.bool,
  notFoundText: PropTypes.string,
  customItemId: PropTypes.string
};

Grid.defaultProps = {
  columns: [],
  gridFilters: {
    gridId: null,
    columns: []
  },
  grid: {
    items: [],
    totalPages: null,
    columns: []
  },
  sorting: {},
  user: {},
  handlePaginationClick: () => {},
  changeSortingColumn: () => {},
  gridId: null,
  filterColumns: false,
  notFoundText: 'Элементы не найдены',
  customItemId: 'id'
};

const enhance = compose(
  connect((state, props) => {
    const { gridId } = props;
    return {
      gridFilters: state.gridsFilters.find(
        gridFilter => gridFilter.gridId === gridId
      )
    };
  })
);

export default enhance(Grid);
