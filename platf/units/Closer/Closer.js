import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components';
import React from 'react';
import Box from '@units/Box/Box';
import PropTypes from 'prop-types';


const StyledCloseIcon = styled(CloseIcon)`
  && {
    transition: color .5s ease;
  }
  &:hover {
    color: red;
    cursor: pointer;
  }
`;

const Closer = ({ onClick, ...restProps }) => <Box onClick={onClick}><StyledCloseIcon {...restProps} /></Box>;

Closer.propTypes = {
  onClick: PropTypes.func,
};

Closer.defaultProps = {
  onClick: () => {},
};

export default Closer;
