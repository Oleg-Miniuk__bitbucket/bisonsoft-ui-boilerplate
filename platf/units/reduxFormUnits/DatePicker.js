import MomentUtils from '@date-io/moment';
import {
  DatePicker as DefaultDatePicker,
  MuiPickersUtilsProvider
} from '@material-ui/pickers';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import { Field } from 'redux-form';
import styled from 'styled-components';

const StyledDefaultDatePicker = styled(DefaultDatePicker)`
  fieldset {
    ${props => (props.isimportant && (!props.value || !props.value.toString().length || props.value === null)) && `border-color: ${props.theme.colors.error};`}
  }
`;

const ConnectedDatePicker = ({
  input: { value, onChange: inputOnChange },
  handleChange,
  isImportant,
  ...restProps
}) => {
  let tempMoment;
  let formatedValue;
  const onInputChange = newDate => {
    tempMoment = newDate;
    // save value in redux, then send to api in 'YYYY.MM.DD'
    const formatedReduxDate = newDate ? moment(newDate, 'YYYY.MM.DD').format('YYYY.MM.DD') : null;
    if (handleChange) {
      handleChange(formatedReduxDate);
    }
    inputOnChange(formatedReduxDate);
  };
  if (!tempMoment && value) {
    // initial with value
    formatedValue = moment(value, 'YYYY.MM.DD');
  } else if (tempMoment) {
    // onChange
    formatedValue = moment(tempMoment, 'YYYY.MM.DD');
  } else {
    // initial without value
    formatedValue = null;
  }
  return (
    <MuiPickersUtilsProvider utils={MomentUtils} locale={moment.locale('ru')}>
      <StyledDefaultDatePicker
        value={formatedValue}
        onChange={onInputChange}
        animateYearScrolling
        format="DD.MM.YYYY"
        okLabel="Подтвердить"
        clearLabel="Очистить"
        cancelLabel="Отменить"
        isimportant={isImportant ? 1 : null}
        {...restProps}
      />
    </MuiPickersUtilsProvider>
  );
};

ConnectedDatePicker.propTypes = {
  input: PropTypes.shape({
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
  }).isRequired,
  handleChange: PropTypes.func,
  isImportant: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string
  ]),
};

ConnectedDatePicker.defaultProps = {
  handleChange: null,
  isImportant: null
};

const DatePicker = ({ name, isImportant, importantText, label, ...restProps }) => (
  <>
    <Field name={name} label={isImportant ? `${label}*` : label} isImportant={isImportant} component={ConnectedDatePicker} {...restProps} />
  </>
);

DatePicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isImportant: PropTypes.bool,
  importantText: PropTypes.string
};

DatePicker.defaultProps = {
  isImportant: null,
  importantText: null
};
export default DatePicker;
