import React, { useState } from 'react';
import MUTextField from '@material-ui/core/TextField';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const StyledMUTextField = styled(MUTextField)`
  fieldset {
    ${props => (props.isimportant && (!props.value || !props.value.length || props.value === null)) && `border-color: ${props.theme.colors.error};`}
  }
`;


const ConnectedTextField = ({
  input,
  meta: { touched, error },
  delay,
  handleChangeTimeout,
  isImportant,
  ...custom
}) => {
  if (handleChangeTimeout) {
    const [changeTimeoutId, setChangeTimeout] = useState(null);
    const onInputChange = event => {
      const { value } = event.target;

      if (delay) {
        clearTimeout(changeTimeoutId);
        setChangeTimeout(
          setTimeout(() => {
            handleChangeTimeout(value);
          }, delay)
        );
        input.onChange(value);
      } else {
        handleChangeTimeout(value);
        input.onChange(value);
      }
    };
    return (
      <StyledMUTextField
        error={touched && error ? Boolean(error) : null}
        helperText={touched && error ? error : null}
        isimportant={isImportant ? 1 : null}
        {...input}
        {...custom}
        onChange={onInputChange}
      />
    );
  }
  return (
    <StyledMUTextField
      // error={Boolean(error)}
      error={touched && error ? Boolean(error) : null}
      helperText={touched && error ? error : null}
      isimportant={isImportant ? 1 : null}
      {...input}
      {...custom}
    />
  );
};

ConnectedTextField.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string
  }).isRequired,
  delay: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isImportant: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  handleChangeTimeout: PropTypes.func
};
ConnectedTextField.defaultProps = {
  handleChangeTimeout: null,
  delay: null,
  isImportant: null
};
const TextField = ({
  name,
  isImportant,
  importantText,
  validate,
  delay,
  handleChangeTimeout,
  label,
  ...restProps
}) => (
  <>
    <Field
      name={name}
      component={ConnectedTextField}
      validate={validate}
      delay={delay}
      handleChangeTimeout={handleChangeTimeout}
      label={isImportant ? `${label}*` : label}
      isImportant={isImportant}
      {...restProps}
    />
  </>
);

TextField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isImportant: PropTypes.bool,
  importantText: PropTypes.string,
  validate: PropTypes.func,
  delay: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  handleChangeTimeout: PropTypes.func
};

TextField.defaultProps = {
  isImportant: null,
  importantText: null,
  validate: null,
  delay: null,
  handleChangeTimeout: null
};

export default TextField;
