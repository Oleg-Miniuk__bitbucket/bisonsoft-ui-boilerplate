import React from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import CreatableSelect from 'react-select/creatable';
import DefaultSelect, { components as DefaultRSComponents } from 'react-select';
import styled from 'styled-components';
import FormHelperText from '@material-ui/core/FormHelperText';
import muiTheme from '@styles/theme';
import Flex from '../Flex/Flex';
import Box from '../Box/Box';

const DefaultValueContainer = ({ children, cx, ...restProps }) => (
  <Flex
    flexGrow="1"
    p="0 0 0 .7rem"
    align="center"
    w="calc(100% - 4.6rem)"
    minh="3.45rem"
    position="relative"
    className={cx('ValueContainer')}
    {...restProps}
  >
    {children}
  </Flex>
);
const StyledMenu = styled(Box).attrs({
  w: '100%',
  top: '100%',
  position: 'absolute'
})`
  z-index: 2;
`;

const SelectLabel = styled(FormHelperText)`
  display: block;
  padding: 0 5px;
  background-color: white;
  transform: translate(10px, -13px);
  z-index: 1;
  position: absolute;
  && {
    color: inherit;
  }
`;

const DefaultMenuContainer = ({ children, ...restProps }) => (
  <StyledMenu>
    <DefaultRSComponents.Menu {...restProps}>
      {children}
    </DefaultRSComponents.Menu>
  </StyledMenu>
);

DefaultValueContainer.propTypes = {
  children: PropTypes.node,
  cx: PropTypes.func
};

DefaultValueContainer.defaultProps = {
  children: PropTypes.node,
  cx: null
};

DefaultMenuContainer.propTypes = {
  children: PropTypes.node
};

DefaultMenuContainer.defaultProps = {
  children: PropTypes.node
};
const ConnectedSelect = ({
  input,
  options,
  creatable,
  valueProps,
  customComponents,
  handleChange,
  handleInputChange,
  meta: { error },
  isImportant,
  isMulti,
  delay,
  ...restProps
}) => {
  const onChange = value => {
    const newValue = (value || (typeof value === 'number' && !Number.isNaN(value))) && isMulti
      ? value : (value || (typeof value === 'number' && !Number.isNaN(value)))
        ? value.value : null;

    if (handleChange) {
      handleChange(newValue);
    }

    input.onChange(newValue);
  };


  let timeoutId = null;
  const onInputChange = newValue => {
    if (handleInputChange) {
      if (delay) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => {
          handleInputChange(newValue);
        }, delay);
      } else {
        handleInputChange(newValue);
      }
    }
  };

  const formatCreateLabel = value => `Добавить ${value}`;

  const ValueContainer = defaultValueContainerProps => (
    <DefaultValueContainer {...defaultValueContainerProps} {...valueProps} />
  );
  const Menu = defaultMenuProps => (
    <DefaultMenuContainer {...defaultMenuProps} {...valueProps} />
  );
  const components = {
    Menu,
    ValueContainer,
    ...customComponents
  };
  const customStyles = {
    control: (provided, state) => ({
      ...provided,
      boxShadow:
        isImportant
        && (!input.value || !input.value.length || input.value === null)
        && !state.isFocused
          ? 'none'
          : state.isFocused
            && `0px 0px 0px 1px ${muiTheme.palette.primary.main}`,
      borderColor:
        isImportant
        && (!input.value || !input.value.length || input.value === null)
        && !state.isFocused
          ? `${muiTheme.palette.error.main}`
          : state.isFocused
            ? `${muiTheme.palette.primary.main}`
            : state.isDisabled
              ? 'rgba(0, 0, 0, 0.26)'
              : 'rgba(0, 0, 0, 0.23)',
      backgroundColor: state.isDisabled && 'transparent'
    }),
    placeholder: provided => ({
      ...provided,
      fontSize: '1rem',
      color: `${muiTheme.palette.text.primary}`
    }),
    singleValue: provided => ({
      ...provided,
      fontSize: '1rem',
      letterSpacing: 'normal'
    })
  };

  const getCreatableValue = () => {
    if (options.length) {
      return options.filter(option => option.value === input.value);
    }
    if (input.value) {
      return {
        value: input.value,
        label: input.value
      };
    }
    return null;
  };

  const getDefaultValue = () => {
    if (!isMulti) {
      return options.filter(option => option.value === input.value);
    }
    return input.value;
  };

  if (creatable) {
    return (
      <>
        {(input.value || input.value === 0 || input.value.length > 0) && (
          <SelectLabel>{restProps.placeholder}</SelectLabel>
        )}
        <CreatableSelect
          components={components}
          options={options}
          isClearable
          onChange={onChange}
          onInputChange={onInputChange}
          formatCreateLabel={formatCreateLabel}
          value={getCreatableValue()}
          error={Boolean(error)}
          helperText={error || null}
          {...restProps}
          styles={customStyles}
          theme={theme => ({
            ...theme,
            colors: {
              ...theme.colors,
              neutral30: `${muiTheme.palette.text.primary}`
            }
          })}
          isMulti={isMulti}
        />
        {error && <FormHelperText error>{error}</FormHelperText>}
      </>
    );
  }
  return (
    <>
      {(input.value || input.value === 0 || input.value.length > 0) && (
        <SelectLabel>{restProps.placeholder}</SelectLabel>
      )}
      <DefaultSelect
        components={components}
        options={options}
        isClearable
        onChange={onChange}
        onInputChange={onInputChange}
        value={getDefaultValue()}
        {...restProps}
        styles={customStyles}
        theme={theme => ({
          ...theme,
          colors: {
            ...theme.colors,
            neutral30: `${muiTheme.palette.text.primary}`
          }
        })}
        isMulti={isMulti}
      />
    </>
  );
};

ConnectedSelect.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string
  }).isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
    })
  ),
  creatable: PropTypes.bool,
  valueProps: PropTypes.shape({}),
  customComponents: PropTypes.shape({}),
  handleChange: PropTypes.func,
  handleInputChange: PropTypes.func,
  isImportant: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  isMulti: PropTypes.bool,
  delay: PropTypes.number,
};

ConnectedSelect.defaultProps = {
  options: [],
  creatable: false,
  valueProps: null,
  customComponents: null,
  handleChange: null,
  handleInputChange: null,
  isImportant: null,
  isMulti: false,
  delay: null
};

const Select = ({
  validate,
  name,
  isImportant,
  importantText,
  placeholder,
  ...restProps
}) => (
  <>
    <Field
      validate={validate}
      name={name}
      component={ConnectedSelect}
      {...restProps}
      placeholder={isImportant ? `${placeholder}*` : placeholder}
      isImportant={isImportant}
    />
  </>
);

Select.propTypes = {
  name: PropTypes.string.isRequired,
  isImportant: PropTypes.bool,
  placeholder: PropTypes.string.isRequired,
  importantText: PropTypes.string,
  validate: PropTypes.func
};
Select.defaultProps = {
  isImportant: null,
  importantText: null,
  validate: null
};

export default Select;
