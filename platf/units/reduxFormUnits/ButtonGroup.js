import React from 'react';
import { Field } from 'redux-form';
import MUButtonGroup from '@material-ui/core/ButtonGroup';
import PropTypes from 'prop-types';


const ConnectedButtonGroup = ({ input: reduxFormInput, children }) => {
  const getHandleBtnClick = el => () => {
    const { props: { onClick, btnName } = {} } = el;
    reduxFormInput.onChange(btnName);
    if (typeof onClick === 'function') {
      onClick();
    }
  };

  const renderBtnChild = el => {
    const color = reduxFormInput.value === el.props.btnName ? 'primary' : 'default';
    return React.cloneElement(el, {
      onClick: getHandleBtnClick(el),
      color,
      variant: 'contained'
    });
  };

  return (
    <MUButtonGroup>
      {React.Children.map(children, renderBtnChild)}
    </MUButtonGroup>
  );
};

ConnectedButtonGroup.propTypes = {
  children: PropTypes.node.isRequired,
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
  }).isRequired
};


const ButtonGroup = ({ name, ...restProps }) => (
  <Field name={name} component={ConnectedButtonGroup} {...restProps} />
);

ButtonGroup.propTypes = {
  name: PropTypes.string.isRequired
};

export default ButtonGroup;
