import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Button } from '@material-ui/core';
import { getFormSyncErrors } from 'redux-form';
import loaderController from '@controllers/loader/loaderController';

class FormButton extends Component {
  state = {
    buttonIsDisable: false
  };

  componentDidMount() {
    this.setButtonState();
  }

  componentDidUpdate() {
    this.setButtonState();
  }

  setButtonState = async () => {
    const { buttonIsDisable } = this.state;
    const buttonState = await this.shouldDisableButton();
    if (buttonIsDisable !== buttonState) {
      this.setState({
        buttonIsDisable: buttonState
      });
    }
  };

  handleClickButton = async event => {
    const { handleClick, dispatch } = this.props;
    if (typeof handleClick === 'function') {
      loaderController.setAppPending(dispatch);
      await handleClick(event);
      loaderController.unSetAppPending(dispatch);
    }
  };

  shouldDisableButton = () => {
    const {
      form,
      requiredFields,
      syncErrors,
      exceptionFields,
      customDisabledButtonRule
    } = this.props;
    const { registeredFields = {}, values } = form;

    if (customDisabledButtonRule && customDisabledButtonRule()) {
      return true;
    }
    if (values && !Object.keys(syncErrors).length) {
      if (requiredFields.length) {
        return !!requiredFields.filter(
          requiredField => !exceptionFields.includes(requiredField)
            && (!Object.keys(values).includes(requiredField)
              || !`${values[requiredField]}`.length || values[requiredField] === null)
        ).length;
      }

      const registeredFieldsArray = Object.keys(registeredFields);

      return !!registeredFieldsArray.filter(
        requiredField => !exceptionFields.includes(requiredField)
          && (!Object.keys(values).includes(requiredField)
            || !`${values[requiredField]}`.length || values[requiredField] === null)
      ).length;
    }
    return true;
  };

  render() {
    const {
      color,
      variant,
      children,
      type,
      loader: { isPending = false }
    } = this.props;
    const { buttonIsDisable } = this.state;
    return (
      <Button
        disabled={buttonIsDisable || isPending}
        color={color}
        variant={variant}
        onClick={this.handleClickButton}
        type={type}
        children={children}
      />
    );
  }
}

FormButton.propTypes = {
  requiredFields: PropTypes.arrayOf(PropTypes.string),
  form: PropTypes.shape(),
  color: PropTypes.string,
  variant: PropTypes.string,
  handleClick: PropTypes.func,
  children: PropTypes.string.isRequired,
  loader: PropTypes.shape(),
  syncErrors: PropTypes.shape(),
  exceptionFields: PropTypes.arrayOf(PropTypes.string),
  customDisabledButtonRule: PropTypes.func,
  type: PropTypes.string,
  dispatch: PropTypes.func.isRequired
};

FormButton.defaultProps = {
  requiredFields: [],
  form: {},
  color: 'primary',
  variant: 'contained',
  handleClick: null,
  loader: {},
  syncErrors: null,
  exceptionFields: [],
  customDisabledButtonRule: null,
  type: 'button'
};

const enhance = compose(
  connect((state, props) => ({
    loader: state.loader,
    form: state.form[props.formId],
    syncErrors: getFormSyncErrors(props.formId)(state)
  }))
);

export default enhance(FormButton);
