import React from 'react';
import MUFormControlLabel from '@material-ui/core/FormControlLabel';
import MUCheckbox from '@material-ui/core/Checkbox';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const ConnectedCheckbox = ({ input, color, label, onChange, disabled, ...custom }) => {
  const onClick = () => {
    input.onChange(!input.value);
    if (onChange && typeof onChange === 'function') {
      onChange(!input.value);
    }
  };

  const getLabelAddProps = ({ isDisabled, elemColor }) => {
    if (!isDisabled) {
      if (elemColor === 'primary') return { color: 'textPrimary' };
      if (elemColor === 'secondary') return { color: 'textSecondary' };
    }
    return {};
  };
  const labelAddProps = getLabelAddProps({ isDisabled: disabled, elemColor: color });
  return (
    <MUFormControlLabel
      control={(
        <MUCheckbox
          checked={!!input.value}
          color={color}
          onClick={onClick}
          disabled={disabled}
          {...custom}
        />
      )}
      label={(
        <Typography {...labelAddProps}>
          {label}
        </Typography>
      )}
    />
  );
};

ConnectedCheckbox.propTypes = {
  input: PropTypes.shape({}).isRequired,
  color: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]).isRequired,
  disabled: PropTypes.bool
};
ConnectedCheckbox.defaultProps = {
  color: 'primary',
  disabled: null
};

const Checkbox = ({ name, ...restProps }) => (
  <Field name={name} component={ConnectedCheckbox} {...restProps} />
);
Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]).isRequired
};

export default Checkbox;
