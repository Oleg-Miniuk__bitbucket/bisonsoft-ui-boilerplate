import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { RadioGroup as MuiRadioGroup } from '@material-ui/core';
import Radio from '@material-ui/core/Radio';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';
import styled from 'styled-components';
import FormLabel from '@material-ui/core/FormLabel';

const FormControlLabel = styled(MuiFormControlLabel)`
  ${props => !props.row && 'margin-bottom: .7rem; margin-top: .7rem;'
}
`;

const ConnectedRadioGroup = ({
  input,
  options,
  currentValue,
  fieldName,
  valueProps,
  customComponents,
  handleEachRadioChange,
  row,
  label,
  ...restProps
}) => {
  const onChange = event => {
    if (handleEachRadioChange) {
      handleEachRadioChange(event.target.value);
    }
    input.onChange(event.target.value);
  };
  return (
    <>
      {label && (
        <FormLabel component="legend">{label}</FormLabel>
      )}
      <MuiRadioGroup name={fieldName} value={currentValue} onChange={onChange} row={row}>
        {options.map(option => <FormControlLabel key={option.value} value={option.value} control={<Radio {...restProps} />} label={option.label} row={row ? 1 : null} />
        )}
      </MuiRadioGroup>
    </>
  );
};

ConnectedRadioGroup.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired
  }).isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired
    })
  ),
  currentValue: PropTypes.string,
  valueProps: PropTypes.shape({}),
  customComponents: PropTypes.shape({}),
  handleEachRadioChange: PropTypes.func,
  fieldName: PropTypes.string.isRequired,
  row: PropTypes.bool.isRequired,
  label: PropTypes.string
};

ConnectedRadioGroup.defaultProps = {
  options: [],
  valueProps: null,
  customComponents: null,
  handleEachRadioChange: null,
  currentValue: null,
  label: null
};

const RadioGroup = ({ name, currentValue, row, ...restProps }) => <Field name={name} fieldName={name} row={row} currentValue={currentValue} component={ConnectedRadioGroup} {...restProps} />;

RadioGroup.propTypes = {
  name: PropTypes.string.isRequired,
  currentValue: PropTypes.string,
  row: PropTypes.bool
};
RadioGroup.defaultProps = {
  currentValue: null,
  row: false
};

export default RadioGroup;
