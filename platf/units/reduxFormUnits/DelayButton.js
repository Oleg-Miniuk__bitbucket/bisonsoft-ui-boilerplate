import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import loaderController from '@controllers/loader/loaderController';
import Box from '../Box/Box';

class DelayButton extends Component {
  state = {
    timer: 0
  };

  buttonTimeout = null;

  startTimer = () => {
    const { delay } = this.props;
    this.setState({
      timer: delay
    });
    this.buttonTimeout = setInterval(() => {
      const { timer } = this.state;
      this.setState({
        timer: timer - 1
      });
      if (timer === 0) {
        this.endTimer();
      }
    }, 1000);
  };

  endTimer = () => {
    clearInterval(this.buttonTimeout);
    this.setState({
      timer: 0
    });
  };

  handleClick = async () => {
    const { onClick, dispatch } = this.props;

    if (typeof onClick === 'function') {
      loaderController.setAppPending(dispatch);
      await onClick();
      loaderController.unSetAppPending(dispatch);
      this.startTimer();
    }
  };

  componentWillUnmount() {
    clearInterval(this.buttonTimeout);
  }

  render() {
    const {
      loader: { isPending = false },
      customDisabled,
      delayText,
      dispatch,
      ...props
    } = this.props;
    const { timer } = this.state;

    return (
      <>
        <Button
          {...props}
          disabled={timer !== 0 || isPending || customDisabled}
          onClick={this.handleClick}
        />
        {timer > 0 && (
          <Box mt="0.5rem">
            <Typography>
              {delayText}
              {` ${moment.utc(timer * 1000).format('mm:ss')}.`}
            </Typography>
          </Box>
        )}
      </>
    );
  }
}

DelayButton.propTypes = {
  loader: PropTypes.shape(),
  customDisabled: PropTypes.bool,
  onClick: PropTypes.func,
  delay: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  delayText: PropTypes.string
};

DelayButton.defaultProps = {
  loader: {},
  customDisabled: false,
  onClick: null,
  delay: 0,
  delayText: 'Кнопка разбокируется через'
};
const enhance = compose(
  connect(state => ({
    loader: state.loader
  }))
);

export default enhance(DelayButton);
