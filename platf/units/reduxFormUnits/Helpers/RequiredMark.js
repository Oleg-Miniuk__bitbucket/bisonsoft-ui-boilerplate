import FormHelperText from '@material-ui/core/FormHelperText';
import styled from 'styled-components';

export const RequiredMark = styled(FormHelperText)`
  && {
    margin-top: 0.1rem;
    color: ${props => props.theme.colors.warning};
  }
`;
