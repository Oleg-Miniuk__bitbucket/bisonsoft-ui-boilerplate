import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';


const LinearProgressBarWrapper = styled('span')`
  display: block;
  position: relative;
  overflow: hidden;
`;
const PercentTitleWrapper = styled('span')`
  display: block;
  line-height: 1.2;
  font-size: 0;
  ${props => parseFloat(props.percent) && `transform: translateX(${props.percent}%)`}
`;

const PercentTitle = styled(Typography).attrs({
  component: 'span',
  variant: 'inherit'
})`
  display: inline-block;
  line-height: 1.5;
  font-size: .5rem;
  ${props => `${parseFloat(props.percent) > 50 ? 'transform: translateX(-100%)' : false}`}
`;

const LinearProgressBarBack = styled('span')`
  display: block;
  background-color: ${props => props.theme.colors.linearProgressBarBack};
  height: 5px;
  border-radius: 7px;
  position: relative;
`;
const LinearProgressBarFront = styled('span')`
  display: block;
  background-color: ${props => props.theme.colors.linearProgressBarFront};
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  border-radius: 7px;
  ${props => `width: ${parseFloat(props.percent)}%`
}
`;
class LinearProgressBar extends Component {
  render() {
    const { percent, value } = this.props;
    const label = parseInt(value, 10) <= 0 ? 0 : parseFloat(percent) < 1 ? '<1' : percent;
    return (
      <LinearProgressBarWrapper>
        <PercentTitleWrapper component="span" percent={percent}>
          <PercentTitle component="span" percent={percent}>
            {label}
            %
          </PercentTitle>
        </PercentTitleWrapper>
        <LinearProgressBarBack>
          <LinearProgressBarFront percent={percent} />
        </LinearProgressBarBack>

      </LinearProgressBarWrapper>
    );
  }
}
LinearProgressBar.propTypes = {
  percent: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

LinearProgressBar.defaultProps = {
  percent: 0,
  value: 0
};
export default LinearProgressBar;
