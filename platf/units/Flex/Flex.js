import styled from 'styled-components';
import PropTypes from 'prop-types';

import Box from '@units/Box/Box';
import { centered } from '@utils/mixin';

const Flex = styled(Box)`
  display: ${props => (!props.inline ? 'flex' : 'inline-flex')};
  flex-direction: ${props => props.column
    ? props.reverse
      ? 'column-reverse'
      : 'column'
    : props.reverse
      ? 'row-reverse'
      : 'row'};
  ${centered};
  justify-content: ${props => props.justify};
  align-items: ${props => props.align};
  flex-wrap: ${props => (props.flexWrap ? 'wrap' : 'nowrap')};
  ${centered};
`;

Flex.propTypes = {
  column: PropTypes.bool,
  reverse: PropTypes.bool,
  height: PropTypes.string,
  justify: PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'space-between', 'space-around']),
  align: PropTypes.oneOf(['stretch', 'flex-start', 'flex-end', 'center', 'baseline']),
  flexWrap: PropTypes.bool,
  inline: PropTypes.bool
};

Flex.defaultProps = {
  column: false,
  reverse: false,
  justify: 'flex-start',
  align: 'stretch',
  height: 'auto',
  flexWrap: false,
  inline: false
};

export default Flex;
